using System.Collections.Generic;
using System.Linq;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;

namespace HamerSoft.Wizard.Serializable
{
    public static class Serializer
    {
        public static List<SerializableSection> Serialize(Installer installer)
        {
            return installer.Sections.Select(section => Serialize(section.Value)).ToList();
        }

        public static SerializableSection Serialize(Section section)
        {
            return new SerializableSection(
                section.GetType().FullName, section.Options.Select(o => Serialize(o.Key, o.Value)).ToList());
        }

        public static SerializableOption Serialize(string key, ITranspilable option)
        {
            return new SerializableOption
            {
                Type = option.GetType().FullName,
                Key = key,
                Values = option.Values?.Select(v => v.ToString()).ToList() ?? null
            };
        }
    }
}