using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Wizard.Serializable
{
    [Serializable]
    public class SerializableSection
    {
        [SerializeField] public string Name;
        [SerializeField] public string Type;
        [SerializeField] public List<SerializableOption> Options;

        public SerializableSection(string type, List<SerializableOption> options)
        {
            Type = type;
            Name = type.Split('.','+').Last();
            Options = options;
        }
    }
}