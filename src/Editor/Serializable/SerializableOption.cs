using System;
using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.Wizard.Serializable
{

    [Serializable]
    public class SerializableOption
    {
        [SerializeField] public string Type;
        [SerializeField] public string Key;
        [SerializeField] public List<string> Values;
    }
}