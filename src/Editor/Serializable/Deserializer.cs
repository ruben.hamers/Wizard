using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using UnityEngine;

namespace HamerSoft.Wizard.Serializable
{
    public static class Deserializer
    {
        public static Installer Deserialize(Config config)
        {
            var installer = new Installer();
            foreach (SerializableSection section in config.Sections)
            {
                try
                {
                    var resultSection = Deserialize(section);
                    installer.SetSection(resultSection);
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"Failed to deserialize Section {section.Type} with Exception {e} | {e.Message}");
                    throw;
                }
            }

            return installer;
        }

        public static Section Deserialize(SerializableSection section)
        {
            if (string.IsNullOrWhiteSpace(section.Type))
                return null;

            try
            {
                var resultSection = Activator.CreateInstance(Type.GetType(section.Type), false) as Section;
                foreach (var option in section.Options)
                    Deserialize(resultSection, option);

                return resultSection;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        private static void Deserialize(Section resultSection, SerializableOption option)
        {
            try
            {
                resultSection.Options[option.Key] = Deserialize(option);
                if (resultSection.Options[option.Key] == null)
                {
                    resultSection.Options.Remove(option.Key);
                    Debug.LogErrorFormat($"Failed to Deserialize {option.Type} with Key: {option.Key}!");
                }
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat($"Failed to Deserialize {option.Type} with Key: {option.Key}! Exception was thrown: {e} | {e.Message}");
            }
        }

        public static ITranspilable Deserialize(SerializableOption option)
        {
            try
            {
                return Activator.CreateInstance(Type.GetType(option.Type), DeserializeValues(option.Values)) as ITranspilable;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static object[] DeserializeValues(List<string> values)
        {
            if (values == null)
                return null;
            var result = new object[values.Count];
            int index = 0;
            foreach (string value in values)
            {
                if (bool.TryParse(value, out var boolean))
                    result[index] = boolean;
                else if (int.TryParse(value, out var number))
                    result[index] = number;
                else
                    result[index] = value;
                index++;
            }

            return result;
        }
    }
}