﻿using System;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Windows
{
    public class SelectDirectoryWindow : EditorWindow
    {
        public event Action<string> Confirmed;
        public event Action Destroyed;
        private string _buildFolder;

        protected void OnEnable()
        {
            titleContent = new GUIContent("Select your Build folder.");
        }

        protected void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(_buildFolder ?? "Please select a folder.");
            if (GUILayout.Button("Select Folder"))
                _buildFolder = EditorUtility.OpenFolderPanel("Select build folder", @"C:\", "");
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("Confirm"))
            {
                OnConfirmed();
                Close();
                DestroyImmediate(this);
            }
        }

        protected void OnDestroy()
        {
            Destroyed?.Invoke();
        }

        private void OnConfirmed()
        {
            Confirmed?.Invoke(_buildFolder);
        }
    }
}