using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.UiSections;

namespace HamerSoft.Wizard.Ui
{
    public class SectionFactory
    {
        public List<SectionPresenter> CreateUiSections(List<SerializableSection> sections)
        {
            var uiSections = new List<SectionPresenter>();
            foreach (var serializableSection in sections)
            {
                SectionPresenter section = null;
                switch (serializableSection.Name)
                {
                    case "Setup":
                        section = new SetupPresenter(serializableSection);
                        break;
                    case "Files":
                        section = new FilesPresenter(serializableSection);
                        break;
                    case "Icons":
                        section = new IconsPresenter(serializableSection);
                        break;
                    case "Languages":
                        section = new LanguagesPresenter(serializableSection);
                        break;
                    case "Run":
                        section = new RunPresenter(serializableSection);
                        break;
                    case "Tasks":
                        section = new TasksPresenter(serializableSection);
                        break;
                }

                // if (section == null)
                // throw new ArgumentException($"Could not create UISection form with Name {serializableSection.Name}");
                if (section != null)
                    uiSections.Add(section);
            }

            return uiSections;
        }
    }
}