using System;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using HamerSoft.Wizard.Ui.Components.File;

namespace HamerSoft.Wizard.Ui
{
    public static class PresenterFactory
    {
        public static AutoStringPresenter CreateAutoStringPresenter(SerializableOption option, string tooltip, Func<string> autoFn, Func<string, string> validateFn = null)
        {
            return option == null ? null : new AutoStringPresenter(option, tooltip, autoFn, validateFn);
        }

        public static TogglePresenter CreateTogglePresenter(SerializableOption option, string tooltip)
        {
            return option == null ? null : new TogglePresenter(option, tooltip);
        }

        public static SelectionPresenter CreateSelectionPresenter(SerializableOption option, string tooltip, string[] options)
        {
            return option == null ? null : new SelectionPresenter(option, tooltip, options);
        }

        public static FilePresenter CreateFilePresenter(SerializableOption option, string tooltip)
        {
            return option == null ? null : new FilePresenter(option, tooltip);
        }

        public static LanguageTogglePresenter CreateLanguageTogglePresenter(SerializableOption option, string tooltip, bool enabled)
        {
            return option == null ? null : new LanguageTogglePresenter(option, tooltip, enabled);
        }

        public static StringPresenter CreateStringPresenter(SerializableOption option, string tooltip, Func<string, string> validator = null)
        {
            return option == null ? null : new StringPresenter(option, tooltip, validator);
        }

        public static PersistentDataPresenter CreatePersistentDataPresenter(SerializableOption option, string tooltip, Action<PersistentDataPresenter> removeCallback)
        {
            return option == null ? null : new PersistentDataPresenter(option, tooltip, removeCallback);
        }
    }
}