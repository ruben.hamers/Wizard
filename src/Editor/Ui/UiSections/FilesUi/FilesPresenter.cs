using System.Collections.Generic;
using System.Linq;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using UnityEditor;
using UnityEngine;
using String = System.String;

namespace HamerSoft.Wizard.Ui
{
    public class FilesPresenter : SectionPresenter<FilesSection>
    {
        private readonly string _tooltip = "Include this path in the installer.";
        private readonly string _persistentDataPath;

        public FilesPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
            _persistentDataPath = Application.persistentDataPath;
        }

        protected override Dictionary<string, SerializableOption> GetRootDictionary()
        {
            int index = 0;
            return SerializableSection.Options.ToDictionary(key => (index++).ToString(), value => value);
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            return new List<Presenter>(options.Select(o =>
                PresenterFactory.CreatePersistentDataPresenter(o.Value, _tooltip, RemovePresenter)));
        }

        protected override FilesSection CreateSection()
        {
            return new FilesSection();
        }

        private void RemovePresenter(PersistentDataPresenter presenter)
        {
            Components.Remove(presenter);
            SerializableSection.Options.Remove(presenter.Option);
        }

        private SerializableOption CreateNewOption()
        {
            var option = new SerializableOption
            {
                Key = System.Guid.NewGuid().ToString(),
                Type = typeof(PersistentDataOption).FullName,
                Values = new List<string> {_persistentDataPath, String.Empty, true.ToString()}
            };
            SerializableSection.Options.Add(option);
            return option;
        }

        protected override void DrawComponents()
        {
            EditorGUILayout.HelpBox(
                "Default Unity3D compiled files and directories are included automatically!\r\nThis includes the .exe file, UnityEngine.dll, CrashHandler, <project>_Data folder, MonobleedingEdge folder and StreamingAssets if present.",
                MessageType.Info);
            EditorGUILayout.HelpBox($"Note: All paths declared here are LOCAL to {_persistentDataPath}. (Depending on your platform: on Windows this is %userprofile%\\AppData\\LocalLow\\<companyname>\\<productname>)", MessageType.Info);
            EditorGUILayout.HelpBox("An empty directory name means it will include ALL folders and files that are on the PersistentDataPath.", MessageType.Info);
            EditorGUILayout.HelpBox(
                "There is no validation on the paths since they might not be present on the machines while Unity3D is open by different members of your team.\r\nAnd because the PersistentDataPath can be wiped clear of files for, debugging purposes for example.",
                MessageType.Warning);
            if (GUILayout.Button(new GUIContent("Add Directory or File", "Add a new to copy.")))
                Components.Add(PresenterFactory.CreatePersistentDataPresenter(
                    CreateNewOption(), _tooltip, RemovePresenter));
            base.DrawComponents();
        }
    }
}