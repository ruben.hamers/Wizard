namespace HamerSoft.Wizard.Ui
{
    public class RunSection : UiSection
    {
        protected override string Name => "Run";

        public RunSection() : base(false)
        {
        }
    }
}