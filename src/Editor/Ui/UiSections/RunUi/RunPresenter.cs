using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using UnityEditor;

namespace HamerSoft.Wizard.Ui
{
    public class RunPresenter : SectionPresenter<RunSection>
    {
        public RunPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            return new List<Presenter>();
        }

        protected override RunSection CreateSection()
        {
            return new RunSection();
        }

        protected override void DrawComponents()
        {
            EditorGUILayout.HelpBox("Nothing to be configured, yet...", MessageType.Info);
            base.DrawComponents();
        }
    }
}