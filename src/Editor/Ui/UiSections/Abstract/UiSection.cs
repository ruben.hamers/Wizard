using System;
using UnityEditor;

namespace HamerSoft.Wizard.Ui
{
    public interface IDrawable
    {
        void Draw();
    }

    public abstract class UiSection : IDrawable
    {
        public bool IsOpen;
        protected abstract string Name { get; }

        public UiSection(bool isOpen = false)
        {
            IsOpen = isOpen;
        }

        public void Draw()
        {
            EditorGUILayout.BeginVertical();
            IsOpen = EditorGUILayout.BeginFoldoutHeaderGroup(IsOpen, $"[{Name}]");
            EditorGUILayout.EndFoldoutHeaderGroup();
            EditorGUILayout.EndVertical();
        }

        public virtual void DrawOpen(Action draw)
        {
            EditorGUILayout.BeginVertical();
            IsOpen = EditorGUILayout.BeginFoldoutHeaderGroup(IsOpen, $"[{Name}]");
            draw.Invoke();
            EditorGUILayout.EndFoldoutHeaderGroup();
            EditorGUILayout.EndVertical();
        }
    }
}