using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Wizard.Serializable;
using UnityEngine;

namespace HamerSoft.Wizard.Ui
{
    public abstract class SectionPresenter : Presenter
    {
        public abstract SerializableSection GetData();
    }

    public abstract class SectionPresenter<T> : SectionPresenter where T : UiSection
    {
        protected SerializableSection SerializableSection;
        protected List<Presenter> Components;
        protected T UiSection;

        public SectionPresenter(SerializableSection serializableSection)
        {
            SerializableSection = serializableSection;
            Components = CreateComponents(GetRootDictionary()).Where(o => o != null).ToList();
            UiSection = CreateSection();
        }

        protected virtual Dictionary<string, SerializableOption> GetRootDictionary()
        {
            return SerializableSection.Options.ToDictionary(key => key.Key, value => value);
        }

        protected abstract List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options);
        protected abstract T CreateSection();

        public override SerializableSection GetData()
        {
            return SerializableSection;
        }

        public override void Draw()
        {
            if (UiSection.IsOpen)
                UiSection.DrawOpen(DrawComponents);
            else
                UiSection.Draw();
        }

        protected virtual void DrawComponents()
        {
            try
            {
                Components.ForEach(c => c.Draw());
            }
            catch (Exception)
            {
                Debug.LogWarning($"Failed to draw components! Maybe collection got edited!");
            }

        }
    }
}