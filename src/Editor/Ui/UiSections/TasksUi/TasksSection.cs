namespace HamerSoft.Wizard.Ui.UiSections
{
    public class TasksSection : UiSection
    {
        protected override string Name => "Tasks";

        public TasksSection() : base(false)
        {
        }
    }
}