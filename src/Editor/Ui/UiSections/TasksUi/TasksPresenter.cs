using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using UnityEditor;

namespace HamerSoft.Wizard.Ui.UiSections
{
    public class TasksPresenter : SectionPresenter<TasksSection>
    {
        public TasksPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            return new List<Presenter>();
        }

        protected override TasksSection CreateSection()
        {
            return new TasksSection();
        }

        protected override void DrawComponents()
        {
            EditorGUILayout.HelpBox("Nothing to be configured, yet...", MessageType.Info);
            base.DrawComponents();
        }
    }
}