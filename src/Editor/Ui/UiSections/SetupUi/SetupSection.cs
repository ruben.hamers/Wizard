namespace HamerSoft.Wizard.Ui
{
    public class SetupSection : UiSection
    {
        protected override string Name => "Setup";

        public SetupSection(bool isOpen = true) : base(isOpen)
        {
        }
    }
}