using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using UnityEngine;
using static HamerSoft.Wizard.Ui.PresenterFactory;

namespace HamerSoft.Wizard.Ui
{
    public class SetupPresenter : SectionPresenter<SetupSection>
    {
        public SetupPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            return new List<Presenter>
            {
                CreateAutoStringPresenter(options["AppName"], "This is the name of your Application", () => Application.productName),
                CreateAutoStringPresenter(options["AppExeName"], "This will be the name of the .exe file.", () => Application.productName, (value) => value.Replace(".exe", "")),
                CreateAutoStringPresenter(options["AppVersion"], "The version of your app.", () => Application.version),
                CreateAutoStringPresenter(options["AppPublisher"], "The name of the publishing company.", () => Application.companyName),
                CreateAutoStringPresenter(options["AppPublisherUrl"], "Name of your website", () => $"https://{Application.companyName}.com"),
                CreateTogglePresenter(options["Uninstallable"], "Add uninstaller for your app."),
                CreateSelectionPresenter(options["Compression"], "The compression type to use for files in the setup.", new[] {"none", "zip", "lzma"}),
                CreateTogglePresenter(options["SolidCompression"], "This causes all files to be compressed at once instead of separately."),
                CreateFilePresenter(options["SetupIconFile"], "Specifies a custom program icon to use for Setup/Uninstall. (Must be .ico file!)"),
                CreateFilePresenter(options["WizardImageFile"], "Specifies the name of the bitmap file to display on the left side of the wizard (Must be .bmp file!)"),
                CreateFilePresenter(options["WizardSmallImageFile"], "Specifies the name of the bitmap file to display in the upper right corner of the wizard. (Must be .bmp file!)"),
            };
        }

        protected override SetupSection CreateSection()
        {
            return new SetupSection();
        }
    }
}