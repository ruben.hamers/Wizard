using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using HamerSoft.Wizard.Ui.UiSections;
using UnityEditor;
using static HamerSoft.Wizard.Ui.PresenterFactory;

namespace HamerSoft.Wizard.Ui
{
    public class LanguagesPresenter : SectionPresenter<LanguagesSection>
    {
        private readonly string[] _availableLanguages =
        {
            "Armenian",
            "Brazilianportuguese",
            "Catalan",
            "Corsican",
            "Czech",
            "Danish",
            "Dutch",
            "Finnish",
            "French",
            "German",
            "Hebrew",
            "Icelandic",
            "Italian",
            "Japanese",
            "Norwegian",
            "Polish",
            "Portuguese",
            "Russian",
            "Slovak",
            "Slovenian",
            "Spanish",
            "Turkish",
            "Ukrainian",
        };

        private readonly string _tooltip = "Include this language as an option to select during installation.";
        private SerializableOption _english;
        private List<SerializableOption> _englishList;

        public LanguagesPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            _english = options["English"];
            _englishList = new List<SerializableOption> {_english};
            return new List<Presenter>(_availableLanguages.Select(l =>
            {
                if (options.TryGetValue(l, out var option))
                    return CreateLanguageTogglePresenter(option, _tooltip, true);
                else
                    return CreateLanguageTogglePresenter(new SerializableOption {Key = l, Type = _english.Type, Values = new List<string> {l}}, _tooltip, false);
            }).ToList());
        }

        protected override LanguagesSection CreateSection()
        {
            return new LanguagesSection();
        }

        public override void Draw()
        {
            base.Draw();
            SerializableSection = GetData();
        }

        protected override void DrawComponents()
        {
            EditorGUILayout.HelpBox("English is enabled by default, you can add extra language options if you prefer.\r\nIf the language you are looking for is not in the selection, it is not supported.", MessageType.Info);
            base.DrawComponents();
        }

        private void DrawLanguagesAsGrid()
        {
            EditorGUILayout.BeginVertical();
            var begin = true;
            for (int i = 0; i < Components.Count; i++)
            {
                if (i % 3 == 0)
                {
                    if (begin)
                    {
                        EditorGUILayout.BeginHorizontal();
                        begin = false;
                    }
                    else
                    {
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                    }
                }

                Components[i].Draw();
            }

            if (Components.Count % 3 != 0)
                EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        public override SerializableSection GetData()
        {
            var enabledLanguages = Components.Cast<LanguageTogglePresenter>().Where(tp => tp.Drawable.Value || tp.Option.Key.Equals("english", StringComparison.OrdinalIgnoreCase));
            SerializableSection.Options = _englishList.Concat(enabledLanguages.Select(l => l.Option)).ToList(); 
            return SerializableSection;
        }
    }
}