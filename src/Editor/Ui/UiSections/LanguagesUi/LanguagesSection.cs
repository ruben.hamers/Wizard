namespace HamerSoft.Wizard.Ui
{
    public class LanguagesSection : UiSection
    {
        protected override string Name => "Languages";

        public LanguagesSection() : base(false)
        {
        }
    }
}