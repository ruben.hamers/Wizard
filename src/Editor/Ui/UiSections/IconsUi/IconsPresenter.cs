using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using UnityEditor;

namespace HamerSoft.Wizard.Ui
{
    public class IconsPresenter : SectionPresenter<IconsSection>
    {
        public IconsPresenter(SerializableSection serializableSection) : base(serializableSection)
        {
        }

        protected override List<Presenter> CreateComponents(Dictionary<string, SerializableOption> options)
        {
            return new List<Presenter>();
        }

        protected override IconsSection CreateSection()
        {
            return new IconsSection();
        }

        protected override void DrawComponents()
        {
            EditorGUILayout.HelpBox("Nothing to be configured, yet...", MessageType.Info);
            base.DrawComponents();
        }
    }
}