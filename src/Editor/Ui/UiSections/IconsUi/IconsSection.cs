namespace HamerSoft.Wizard.Ui
{
    public class IconsSection : UiSection
    {
        protected override string Name => "Icons";

        public IconsSection() : base(false)
        {
        }
    }
}