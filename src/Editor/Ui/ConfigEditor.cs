using System.Collections.Generic;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Ui.UiSections;
using UnityEditor;

namespace HamerSoft.Wizard.Ui
{
    [CustomEditor(typeof(Config), true)]
    public class ConfigEditor : UnityEditor.Editor
    {
        private List<SectionPresenter> _sections;
        private Config _config;

        private void OnEnable()
        {
            _config = target as Config;
            var factory = new SectionFactory();
            _sections = factory.CreateUiSections(_config.Sections);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUI.BeginChangeCheck();

            foreach (var section in _sections)
            {
                section?.Draw();
                EditorGUILayout.Separator();
                EditorGUILayout.Space();
            }

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(_config);
                AssetDatabase.SaveAssets();
            }
        }
    }
}