using System;
using HamerSoft.Wizard.Serializable;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class PersistentDataPresenter : UiComponentPresenter<PersistentData, Tuple<string, bool>>
    {
        private readonly Action<PersistentDataPresenter> _removeCallback;

        public PersistentDataPresenter(SerializableOption option, string tooltip, Action<PersistentDataPresenter> removeCallback) : base(option, tooltip)
        {
            _removeCallback = removeCallback;
            Drawable = CreateComponent(option);
            Drawable.SetToolTip(tooltip);
        }

        protected override PersistentData CreateComponent(SerializableOption option)
        {
            if (bool.TryParse(option.Values[2], out var overwrite))
                return new PersistentData("Persistent Data:", new Tuple<string, bool>(option.Values[1], overwrite), () => _removeCallback?.Invoke(this));
            else
            {
                Debug.LogWarning($"Could not convert {option.Values[2]} to a bool! Set overwrite to false in Persistent Data: {option.Values[1]}");
                return new PersistentData("Persistent Data:", new Tuple<string, bool>(option.Values[1], false), () => _removeCallback?.Invoke(this));
            }
        }

        protected override void SetData(Tuple<string, bool> value)
        {
            Option.Values[1] = value.Item1;
            Option.Values[2] = value.Item2.ToString();
        }
    }
}