using System;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class PersistentData : UiComponent<Tuple<string, bool>>
    {
        private string _directory;
        private bool _overwrite;
        private Action _removeCallback;

        public PersistentData(string label, Tuple<string, bool> value, Action removeCallback) : base(label, value)
        {
            _removeCallback = removeCallback;
            _directory = value.Item1;
            _overwrite = value.Item2;
        }

        public override void Draw()
        {
            EditorGUILayout.BeginHorizontal();
            _overwrite = GUILayout.Toggle(_overwrite, new GUIContent("Overwrite: ", $"Overwrite this directory on the PersistentDataPath or only append it?"), GUILayout.ExpandWidth(false));
            GUILayout.Label(new GUIContent(Label, Tooltip));
            _directory = GUILayout.TextField(_directory);
            if (GUILayout.Button(new GUIContent("Delete", "Remove this directory from the installer.")))
                _removeCallback?.Invoke();
            EditorGUILayout.EndHorizontal();
            Value = new Tuple<string, bool>(_directory, _overwrite);
        }
    }
}