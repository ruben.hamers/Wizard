using System;
using HamerSoft.Wizard.Serializable;

namespace HamerSoft.Wizard.Ui.Components
{
    public class StringPresenter : UiComponentPresenter<String, string>
    {
        public StringPresenter(SerializableOption option, string tooltip, Func<string, string> validator = null) : base(option, tooltip)
        {
            Drawable = CreateComponent(option);
            SetToolTip(tooltip);
            SetValidator(validator);
        }

        protected override String CreateComponent(SerializableOption option)
        {
            return new String(option.Key, option.Values[1]);
        }

        protected override void SetData(string value)
        {
            Option.Values[1] = value;
        }
    }
}