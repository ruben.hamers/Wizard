using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class String : UiComponent<string>
    {
        public String(string label, string value) : base(label, value)
        {
        }

        public override void Draw()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent(Label, Tooltip));
            Value = GUILayout.TextField(Value);
            EditorGUILayout.EndHorizontal();
        }
    }
}