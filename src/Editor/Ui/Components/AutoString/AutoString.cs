using System;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class AutoString : UiComponent<string>
    {
        public bool Automatic;

        public AutoString(string label, string value) : base(label, value)
        {
        }

        public override void Draw()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            var auto = Automatic;
            Automatic = GUILayout.Toggle(Automatic, new GUIContent("Automatic: ", $"Load the {Label} automatically"), GUILayout.ExpandWidth(false));
            GUILayout.Label(new GUIContent($"{Label}:", Tooltip));
            if (Automatic)
                GUILayout.Label(Value, GUILayout.ExpandWidth(false));
            else
                Value = GUILayout.TextField(Value, GUILayout.ExpandWidth(false));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
    }
}