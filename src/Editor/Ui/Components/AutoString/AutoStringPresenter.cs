using System;
using HamerSoft.Wizard.Serializable;

namespace HamerSoft.Wizard.Ui.Components
{
    public class AutoStringPresenter : UiComponentPresenter<AutoString, string>
    {
        private readonly Func<string> _autoFn;

        public AutoStringPresenter(SerializableOption option, string tooltip, Func<string> autoFn, Func<string, string> validator = null) : base(option, tooltip)
        {
            _autoFn = autoFn;
            Drawable = CreateComponent(option);
            SetToolTip(tooltip);
            SetValidator(validator);
            var autoValue = autoFn();
            Drawable.Automatic = Drawable.Value == autoValue;
            if (Drawable.Automatic)
                Value = autoValue;
        }

        protected override AutoString CreateComponent(SerializableOption option)
        {
            return new AutoString(option.Key, option.Values[1]);
        }

        public override void Draw()
        {
            var preDrawAuto = Drawable.Automatic;
            base.Draw();
            if (preDrawAuto != Drawable.Automatic && Drawable.Automatic)
                Value = _autoFn();
            SetData(Value);
        }
    }
}