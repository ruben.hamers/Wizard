using HamerSoft.Wizard.Serializable;

namespace HamerSoft.Wizard.Ui.Components
{
    public class LanguageTogglePresenter : UiComponentPresenter<Toggle, bool>
    {
        public bool Enabled { get; private set; }

        public LanguageTogglePresenter(SerializableOption option, string tooltip, bool enabled) : base(option, tooltip)
        {
            Enabled = enabled;
            Drawable = CreateComponent(option);
            SetToolTip(tooltip);
        }

        protected override void SetData(bool value)
        {
            Enabled = value;
        }

        protected override Toggle CreateComponent(SerializableOption option)
        {
            return new Toggle(option.Key, Enabled);
        }
    }
}