using System.Reflection;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class ComponentTest<T, TComponent, TValue> where T : UiComponentPresenter<TComponent, TValue> where TComponent : UiComponent<TValue>
    {
        protected T Presenter;

        protected void ForceSetData(TValue value)
        {
            typeof(T).GetMethod("SetData", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(Presenter, new object[] {value});
        }
    }
}