using System;
using HamerSoft.Wizard.Serializable;

namespace HamerSoft.Wizard.Ui.Components
{
    public abstract class UiComponentPresenter<T, TValue> : Presenter<T> where T : UiComponent<TValue>
    {
        protected readonly string Tooltip;
        public SerializableOption Option { get; }
        protected TValue Value;
        private Func<TValue, TValue> _validateFn;

        public UiComponentPresenter(SerializableOption option, string tooltip)
        {
            Tooltip = tooltip;
            Option = option;
        }

        protected abstract T CreateComponent(SerializableOption option);

        public void SetToolTip(string tooltip)
        {
            Drawable?.SetToolTip(tooltip);
        }

        public void SetValidator(Func<TValue, TValue> validateFn)
        {
            _validateFn = validateFn;
        }

        public override void Draw()
        {
            if (Drawable == null)
                return;
            base.Draw();
            Value = Drawable.Value;
            SetData(Value);
        }

        protected virtual void SetData(TValue value)
        {
            TValue val = _validateFn != null ? _validateFn(value) : value;
            if (val != null)
                Option.Values[1] = val.ToString();
        }
    }
}