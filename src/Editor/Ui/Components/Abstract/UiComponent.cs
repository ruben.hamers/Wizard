using System;

namespace HamerSoft.Wizard.Ui.Components
{
    public abstract class UiComponent : IDrawable
    {
        protected string Tooltip;
        public abstract void Draw();
        public string Label { get; protected set; }

        public UiComponent(string label)
        {
            Label = label;
        }

        public UiComponent SetToolTip(string tooltip)
        {
            Tooltip = tooltip;
            return this;
        }
    }

    public abstract class UiComponent<T> : UiComponent
    {
        public T Value { get; protected set; }

        public UiComponent(string label, T value) : base(label)
        {
            Value = value;
        }

        public new UiComponent<T> SetToolTip(string tooltip)
        {
            Tooltip = tooltip;
            return this;
        }
    }
}