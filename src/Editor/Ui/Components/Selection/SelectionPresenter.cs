using System.Linq;
using HamerSoft.Wizard.Serializable;

namespace HamerSoft.Wizard.Ui.Components
{
    public class SelectionPresenter : UiComponentPresenter<Selection, int>
    {
        private readonly string[] _options;

        public SelectionPresenter(SerializableOption option, string tooltip, string[] options) : base(option, tooltip)
        {
            _options = options ?? new string[1];
            Drawable = CreateComponent(option);
            Drawable.SetToolTip(tooltip);
        }

        protected override Selection CreateComponent(SerializableOption option)
        {
            int selected = _options.ToList().IndexOf(option.Values[1]);
            return new Selection(option.Key, selected < 0 ? 0 : selected, _options);
        }

        protected override void SetData(int value)
        {
            Option.Values[1] = _options[value];
        }
    }
}