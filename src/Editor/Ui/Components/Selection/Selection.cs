using System.Linq;
using UnityEditor;

namespace HamerSoft.Wizard.Ui.Components
{
    public class Selection : UiComponent<int>
    {
        private readonly string[] _options;

        public Selection(string label, int value, string[] options) : base(label, value)
        {
            _options = options;
            Value = value;
        }

        public override void Draw()
        {
            Value = EditorGUILayout.Popup(Label, Value, _options);
        }
    }
}