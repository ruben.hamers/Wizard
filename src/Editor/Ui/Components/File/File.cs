using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components.File
{
    public class File : UiComponent<Object>
    {
        public File(string label, Object value) : base(label, value)
        {
        }

        public override void Draw()
        {
            EditorGUILayout.BeginVertical();
            Value = EditorGUILayout.ObjectField(new GUIContent(Label, Tooltip), Value, typeof(Object), false);

            if (Value is Texture texture)
            {
                var rect = EditorGUILayout.GetControlRect(false, 50);
                rect.width = 50;
                EditorGUI.DrawPreviewTexture(rect, texture);
            }

            EditorGUILayout.EndVertical();
        }

        public void Reset()
        {
            Value = null;
        }
    }
}