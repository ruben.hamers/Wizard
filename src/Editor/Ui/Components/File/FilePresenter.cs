using HamerSoft.Wizard.Serializable;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components.File
{
    public class FilePresenter : UiComponentPresenter<File, Object>
    {
        private readonly string fileExtension;

        public FilePresenter(SerializableOption option, string tooltip) : base(option, tooltip)
        {
            fileExtension = option.Values[2];
            Drawable = CreateComponent(option);
            Drawable.SetToolTip(tooltip);
        }

        protected override File CreateComponent(SerializableOption option)
        {
            string path = option.Values[1];
            var file = AssetDatabase.LoadAssetAtPath<Object>(path);
            return new File(option.Key, file);
        }
//$"{Application.dataPath}{file.Replace("Assets", "")}";
        protected override void SetData(Object value)
        {
            if (!value)
                Option.Values[1] = string.Empty;
            else
            {
                var file = AssetDatabase.GetAssetPath(value);
                if (file.EndsWith(fileExtension))
                {
                    Option.Values[1] = file;
                }
                else
                {
                    Debug.LogWarning($"Could not set the file for {Option.Key}, please make sure it has {fileExtension} as file extension!");
                    Drawable.Reset();
                }
            }
        }
    }
}