using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class Toggle : UiComponent<bool>
    {
        public Toggle(string label, bool value) : base(label, value)
        {
        }

        public override void Draw()
        {
            Value = GUILayout.Toggle(Value, new GUIContent(Label, Tooltip), GUILayout.ExpandWidth(false));
        }
    }
}