using HamerSoft.Wizard.Serializable;
using UnityEngine;

namespace HamerSoft.Wizard.Ui.Components
{
    public class TogglePresenter : UiComponentPresenter<Toggle, bool>
    {
        public TogglePresenter(SerializableOption option, string tooltip) : base(option, tooltip)
        {
            Drawable = CreateComponent(option);
            SetToolTip(tooltip);
        }

        protected override Toggle CreateComponent(SerializableOption option)
        {
            if (bool.TryParse(option.Values[1], out var toggle))
                return new Toggle(option.Key, toggle);
            Debug.LogWarning($"Could not parse boolean in toggle {option.Key}, value is: {option.Values[1]}");
            return new Toggle(option.Key, false);
        }

        protected override void SetData(bool value)
        {
            Option.Values[1] = value.ToString();
        }
    }
}