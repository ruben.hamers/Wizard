namespace HamerSoft.Wizard.Ui
{
    public abstract class Presenter
    {
        public abstract void Draw();
    }

    public abstract class Presenter<T> : Presenter where T : IDrawable
    {
        public T Drawable { get; protected set; }

        public Presenter()
        {
        }

        public override void Draw()
        {
            Drawable.Draw();
        }
    }
}