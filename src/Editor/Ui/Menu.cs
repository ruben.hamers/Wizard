﻿using System;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Adapter;
using HamerSoft.Wizard.Ui.Windows;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Ui
{
    public class Menu
    {
        [MenuItem("HamerSoft/Wizard/Check for Updates")]
        public static void CheckForUpdates()
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                return;
            }

            InnoAdapter.HasLatestVersion(hasLatest =>
            {
                if (hasLatest)
                {
                    Debug.Log("No updates available, you have the latest version installed!");
                    return;
                }

                if (EditorUtility.DisplayDialog("Update", "There is a new version available, would you like to install it?", "Yes", "No"))
                    ForceUpdate();
                else
                    Debug.Log("Wizard Update available yet not installed!");
            });
        }

        [MenuItem("HamerSoft/Wizard/Force Update")]
        public static void ForceUpdate()
        {
            InnoAdapter.UpdateInnoSetup(success => { Debug.Log($"Update {(success ? "successful" : "failed")}"); }, true);
        }

        [MenuItem("HamerSoft/Wizard/Compile Setup File")]
        public static void CreateSetup()
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                return;
            }

            SelectBuildPath(path =>
            {
                var installer = Config.GetInstaller();
                if (installer.SetBuildDir(path))
                {
                    installer.Run(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                    Cli.ShowDirectory(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                    Debug.Log("<b><color=green> Compile Completed and saved on your Desktop</color></b>");
                }
                else
                {
                    Debug.LogWarning("Failed to compile setup!");
                }
            });
        }

        [MenuItem("HamerSoft/Wizard/WriteOut .iss File")]
        public static void WriteOutInnoFile()
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                return;
            }

            SelectBuildPath(path =>
            {
                var installer = Config.GetInstaller();
                if (installer.SetBuildDir(path))
                {
                    installer.WriteOut(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                    Cli.ShowDirectory(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                    Debug.Log("<b><color=green> WriteOut Completed and saved on your Desktop</color></b>");
                }
                else
                {
                    Debug.LogWarning("Failed to write out .iss file!");
                }
            });
        }

        private static void SelectBuildPath(Action<string> callback)
        {
            SelectDirectoryWindow window = null;

            void PathSelected(string path)
            {
                window.Destroyed -= Destroyed;
                window.Confirmed -= PathSelected;
                if (string.IsNullOrWhiteSpace(path))
                {
                    Debug.Log("No path selected!");
                    return;
                }

                callback?.Invoke(path);
            }

            void Destroyed()
            {
                window.Destroyed -= Destroyed;
                window.Confirmed -= PathSelected;
            }

            window = ScriptableObject.CreateInstance<SelectDirectoryWindow>();
            window.Destroyed += Destroyed;
            window.Confirmed += PathSelected;
            window.Show(true);
        }
    }
}