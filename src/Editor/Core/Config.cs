using System.Collections.Generic;
using System.IO;
using System.Linq;
using HamerSoft.Wizard.Core.Sections;
using HamerSoft.Wizard.Serializable;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.Wizard.Core
{
    public class Config : ScriptableObject
    {
        [HideInInspector] public List<SerializableSection> Sections;

        public static Config Load(string resoucesPath = "Wizard/Config")
        {
            var config = Resources.Load<Config>(resoucesPath);
            return config;
        }

        public static Installer GetInstaller(string path = "Wizard/Config")
        {
            var config = Load(path);
            return config.GetInstaller();
        }

        public Installer GetInstaller()
        {
            var installer = Deserializer.Deserialize(this);
            return installer;
        }

#if UNITY_EDITOR
        [MenuItem("HamerSoft/Wizard/Create Config")]
        public static Config Create()
        {
            return Create("Resources/Wizard");
        }

        public static Config Create(string resourcesPath)
        {
            if (Load() is Config c)
                return c;
            var fullPath = Path.Combine(Application.dataPath, resourcesPath);
            CreateDirectory(fullPath);
            return SaveConfig(CreateConfigInstance(), resourcesPath);
        }


        private static Config CreateConfigInstance()
        {
            var config = ScriptableObject.CreateInstance<Config>();
            config.Sections = Serializer.Serialize(new Installer());
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return config;
        }

        private static Config SaveConfig(Config config, string resourcesPath)
        {
            string filename = Path.Combine("Assets", resourcesPath, "Config.asset");
            AssetDatabase.CreateAsset(config, filename);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return AssetDatabase.LoadAssetAtPath<Config>(filename);
        }

        private static void CreateDirectory(string fullPath)
        {
            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);
        }

#endif
    }
}