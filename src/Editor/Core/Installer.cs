﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using UnityEngine;

namespace HamerSoft.Wizard.Core
{
    public class Installer : ITranspilable
    {
        public Dictionary<Type, Section> Sections;
        public object[] Values { get; }

        public Installer()
        {
            Sections = new Dictionary<Type, Section>()
            {
                {typeof(Setup), new Setup(Application.productName, Application.version, Application.companyName)},
                {typeof(Languages), new Languages()},
                {typeof(Tasks), new Tasks()},
                {typeof(Files), new Files()},
                {typeof(Icons), new Icons()},
                {typeof(Run), new Run()},
            };
        }

        public void SetSection(Section s)
        {
            Sections[s.GetType()] = s;
        }

        public T GetSection<T>() where T : Section
        {
            return Sections?.FirstOrDefault(s => s.Value is T).Value as T;
        }

        public bool SetBuildDir(string buildPath)
        {
            return GetSection<Setup>().SetBuildDir(buildPath);
        }

        public string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<Type, Section> section in Sections)
            {
                if (section.Value == null)
                    continue;

                builder.AppendLine(section.Value.Transpile());
            }

            return builder.ToString();
        }

        public void Run(string outputDir)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                Run(Transpile(), outputDir);
            else
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}! Please use a Windows machine!");
        }

        public void Run(string transpiledInstaller, string outputDir)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                var path = WriteOut(transpiledInstaller, null);
                RunRaw(path, outputDir);
            }
            else
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}! Please use a Windows machine!");
        }

        public static void RunRaw(string path, string outputDir)
        {
            try
            {
                if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    var result = Cli.RunInnoCommand($"/Qp /O\"{outputDir}\" /F\"{Application.productName}-Setup\" \"{path}\"");
                    Debug.Log($"RunRaw Output: {result}");
                }
                else
                    Debug.LogWarning($"Cannot Run Wizard on {Application.platform}! Please use a Windows machine!");
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        public string WriteOut(string outputDir = null)
        {
            return WriteOut(Transpile(), outputDir);
        }

        private string WriteOut(string issFile, string outputDir)
        {
            string path = outputDir ?? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-InnoSetup.iss");
            if (!path.EndsWith(".iss"))
                path = Path.Combine(path, $"{Application.productName}-InnoSetup.iss");
            if (File.Exists(path))
                File.Delete(path);

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(issFile);
                sw.Flush();
                sw.Close();
            }

            return path;
        }
    }
}