using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace HamerSoft.Wizard.Core
{
    public static class Cli
    {
        private const string INNO_32 = @"C:\Program Files (x86)\Inno Setup 6";
        private const string INNO_64 = @"C:\Program Files\Inno Setup 6";

        private const string HELP_ARG = "-wizardHelp";
        private const string BUILD_ARG = "-buildPath";
        private const string OUTPUT_ARG = "-outputDir";
        private const string RAW_ARG = "-raw";
        private const string COMPILE_ARG = "-compile-wizard";
        private const string WRITEOUT_ARG = "-writeout-wizard";

        private static string InnoPath;

        /// <summary>
        /// Make sure the CLI initializes on start, in a build and only in scene indexed 0
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void UnitySceneStarted()
        {
            if (!Application.isEditor && SceneManager.GetActiveScene().buildIndex != 0)
                return;

            if (ArgExists(COMPILE_ARG))
                CompileSetup();
            else if (ArgExists(WRITEOUT_ARG))
                WriteOutInnoFile();
        }

        public static string GetArg(string name)
        {
            var args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == name && args.Length > i + 1)
                {
                    return args[i + 1];
                }
            }

            return null;
        }

        public static bool ArgExists(string arg)
        {
            return System.Environment.GetCommandLineArgs().ToList().Exists(a => a.Equals(arg, StringComparison.InvariantCulture));
        }

        //execute method Compile setup
        public static int CompileSetup()
        {
            var help = ArgExists(HELP_ARG);
            var buildPath = GetArg(BUILD_ARG);
            var outputDir = GetArg(OUTPUT_ARG);
            var innoFile = GetArg(RAW_ARG);
            return CompileSetup(help, buildPath, outputDir, innoFile);
        }

        private static int CompileSetup(bool help, string buildPath, string outputDir, string innoFile)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.Log("You can only run Wizard on a Windows OS");
                return 1;
            }
            else if (help)
            {
                ShowHelp();
                return 0;
            }
            else if (string.IsNullOrWhiteSpace(outputDir))
            {
                ShowHelp();
                Debug.Log("[Error]: OutputDir (-outputDir) must be set!");
                return 1;
            }
            else if (File.Exists(innoFile))
            {
                Installer.RunRaw(innoFile, outputDir);
                return 0;
            }
            else if (!string.IsNullOrWhiteSpace(buildPath))
            {
                var installer = Config.GetInstaller();
                if (installer.SetBuildDir(buildPath))
                {
                    installer.Run(outputDir);
                    return 0;
                }

                Debug.Log("-buildPath does not match with a Unity3D Windows build! Please make sure it is the correct path!");
                return 2;
            }

            ShowHelp();
            return 0;
        }

        private static void ShowHelp()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Welcome to the Wizard help!");
            sb.AppendLine("There are a couple of important parameters you need to remember.");
            sb.AppendLine("------");
            sb.AppendLine("-wizardHelp: Shows this information block.");
            sb.AppendLine("-compile-wizard: keyword to trigger a compile.");
            sb.AppendLine("-writeout-wizard: keyword to trigger a write out of the .iss file.");
            sb.AppendLine("-buildPath: The path where your Unity3D Windows build is currently at.");
            sb.AppendLine("-outputDir: The output directory to place the compiled SetupWizard.");
            sb.AppendLine("-raw: Path to a raw InnoSetup file (.iss) to run.");
            sb.AppendLine("------");
            sb.AppendLine("*     -outputDir is always required.");
            sb.AppendLine("**    order of arguments do not matter!.");
            sb.AppendLine("***   A raw input file always has precedence over a configured Wizard in Unity3D since it is probably set for a reason!.");
            sb.AppendLine("****  All paths must be rooted paths!");
            sb.AppendLine("***** Either -compile-wizard or -writeout-wizard not both.");
            sb.AppendLine();
            sb.AppendLine("An example command for compiling a new setup based on the settings configured in Unity3D would be:");
            sb.AppendLine("-compile-wizard -buildpath <path> -outputDir <myOutputDir>");
            sb.AppendLine();
            sb.AppendLine("Another command to run a pre-existing InnoSetup (.iss) file would be:");
            sb.AppendLine("-compile-wizard -raw <myIssFile> -outputDir <myOutputDir>");
            sb.AppendLine();
            sb.AppendLine("An example command for generating a new .iss file based on the settings configured in Unity3D would be:");
            sb.AppendLine("-writeout-wizard -buildpath <path> -outputDir <myOutputDir>");
            Debug.Log(sb);
        }

        public static int WriteOutInnoFile()
        {
            var help = ArgExists(HELP_ARG);
            var buildPath = GetArg(BUILD_ARG);
            var outputDir = GetArg(OUTPUT_ARG);
            return WriteOutInnoFile(help, buildPath, outputDir);
        }

        private static int WriteOutInnoFile(bool help, string buildPath, string outputDir)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.Log("You can only run Wizard on a Windows OS");
                return 1;
            }
            else if (help)
            {
                ShowHelp();
                return 0;
            }
            else if (string.IsNullOrWhiteSpace(buildPath))
            {
                ShowHelp();
                Debug.Log("[Error]: BuildPath (-buildPath) must be set!");
                return 1;
            }
            else if (string.IsNullOrWhiteSpace(outputDir))
            {
                ShowHelp();
                Debug.Log("[Error]: OutputDir (-outputDir) must be set!");
                return 1;
            }

            var installer = Config.GetInstaller();
            if (installer.SetBuildDir(buildPath))
            {
                installer.WriteOut(outputDir);
                return 0;
            }

            Debug.Log("-buildPath does not match with a Unity3D Windows build! Please make sure it is the correct path!");
            return 2;
        }

        public static string RunInnoCommand(string command)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
                return string.Empty;
            try
            {
                if (!TrySetInnoPath(out InnoPath))
                    throw new CliException("Could Not locate InnoSetup in ProgramFiles or ProgramFiles (x86)!");
                var result = RunCommand($"{InnoPath}\\ISCC.exe", command);
                Debug.Log($"Raw Command Output: {result}");
                return result;
            }
            catch (CliException cli)
            {
                Debug.LogWarning(cli.Message);
                return cli.Message;
            }
        }

        public static void ShowDirectory(string path)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
                return;
            RunCommand("explorer.exe", $"/select,{path}");
        }

        public static string RunCommand(string process, string command, bool waitForIdle = true)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
                return String.Empty;
            Process proc = new Process
            {
                StartInfo =
                {
                    FileName = process,
                    Arguments = command,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                }
            };
            proc.EnableRaisingEvents = true;
            StringBuilder output = new StringBuilder();
            try
            {
                proc.Start();
            }
            catch (Exception e)
            {
                throw new CliException(e.Message);
            }

            proc.WaitForExit();
            while (!proc.StandardOutput.EndOfStream)
                output.AppendLine(proc.StandardOutput.ReadLine());
            string error = proc.StandardError.ReadToEnd();
            while (!proc.HasExited)
            {
            }

            var exitCode = proc.ExitCode;
            proc.Close();
            if (exitCode >= 1 && !string.IsNullOrWhiteSpace(error))
                throw new CliException(error);

            return output.ToString();
        }

        private static bool TrySetInnoPath(out string innoPath)
        {
            innoPath = InnoPath;
            if (!string.IsNullOrWhiteSpace(innoPath))
                return true;
            else if (Directory.Exists(INNO_32))
                InnoPath = INNO_32;
            else if (Directory.Exists(INNO_64))
                InnoPath = INNO_64;
            innoPath = InnoPath;
            return !string.IsNullOrWhiteSpace(innoPath);
        }

        public class CliException : Exception
        {
            public CliException(string message) : base(message)
            {
            }
        }

        public static string GetVersion()
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
                return String.Empty;
            if (TrySetInnoPath(out InnoPath))
            {
                var attributes = FileVersionInfo.GetVersionInfo($"{InnoPath}\\unins000.exe");
                return attributes.ProductVersion;
            }

            return null;
        }

        public static void RunExe(string path, Action<int> exited)
        {
            var process = new Process {StartInfo = {Verb = "runas", FileName = path}};
            process.Start();
            while (!process.HasExited)
            {
            }

            exited.Invoke(process.ExitCode);
        }
    }
}