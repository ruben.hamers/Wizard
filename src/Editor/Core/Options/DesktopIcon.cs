namespace HamerSoft.Wizard.Core.Options
{
    public class DesktopIcon : ITranspilable
    {
        public object[] Values { get; }
        public string Transpile()
        {
            return "Name: \"desktopicon\"; Description: \"{cm:CreateDesktopIcon}\"; GroupDescription: \"{cm:AdditionalIcons}\"; Flags: unchecked";
        }
    }
}