﻿namespace HamerSoft.Wizard.Core.Options
{
    public class StringOption : InstallerOption<string>
    {
        public override object[] Values => new object[] {Key, Value};

        public StringOption(string key, string value) : base(key, value)
        {
        }

        public override string Transpile()
        {
            return $"{Key}={Value}";
        }
    }
}