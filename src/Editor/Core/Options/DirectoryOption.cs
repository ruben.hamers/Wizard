namespace HamerSoft.Wizard.Core.Options
{
    public class DirectoryOption : ITranspilable
    {
        protected string Path;
        protected readonly string Directory;
        protected readonly bool Overwrite;
        public object[] Values => new object[] {Path, Directory, Overwrite};

        public DirectoryOption(string path, string directory, bool overwrite)
        {
            Path = path;
            Directory = directory;
            Overwrite = overwrite;
        }

        public virtual string Transpile()
        {
            return $"Source: \"{Path}\\{Directory}\\*\"; DestDir: \"{{app}}\\{Directory}\"; Flags: recursesubdirs createallsubdirs skipifsourcedoesntexist{(Overwrite ? " ignoreversion" : string.Empty)}";
        }
    }
}