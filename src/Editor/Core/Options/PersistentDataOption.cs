using UnityEngine;

namespace HamerSoft.Wizard.Core.Options
{
    public class PersistentDataOption : DirectoryOption
    {
        public PersistentDataOption(string path, string directory, bool overwrite) : base(path, directory, overwrite)
        {
        }

        public override string Transpile()
        {
            Path = Application.persistentDataPath;
            if (System.IO.Path.HasExtension(Directory))
                return TranspileFile();
            else
                return TranspileDirectory();
        }

        private string TranspileDirectory()
        {
            return $"Source: \"{Path}\\{Directory}\\*\"; DestDir: \"{{userappdata}}\\..\\LocalLow\\{{#MyAppPublisher}}\\{{#MyProductName}}\\{Directory}\"; Flags: skipifsourcedoesntexist recursesubdirs createallsubdirs{(Overwrite ? " ignoreversion" : string.Empty)}";
        }

        private string TranspileFile()
        {
            return $"Source: \"{Path}\\{Directory}\"; DestDir: \"{{userappdata}}\\..\\LocalLow\\{{#MyAppPublisher}}\\{{#MyProductName}}\\{Directory}\"; Flags: skipifsourcedoesntexist{(Overwrite ? " ignoreversion" : string.Empty)}";
        }
    }
}