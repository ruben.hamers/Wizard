﻿namespace HamerSoft.Wizard.Core.Options
{
    public class DefinedOption : InstallerOption<string>, DefinableOption
    {
        public string DefineKey => $"My{Key}";
        public override object[] Values => new object[] {Key, Value, CustomFormatting};

        public DefinedOption(string key, string value) : base(key, value)
        {
        }

        public DefinedOption(string key, string value, string format) : base(key, value, format)
        {
        }

        public string Define()
        {
            return $"#define {DefineKey} \"{string.Format(CustomFormatting, Value)}\"";
        }

        public override string Transpile()
        {
            return $"{Key}={{#{DefineKey}}}";
        }
    }
}