﻿using System;

namespace HamerSoft.Wizard.Core.Options
{
    public abstract class InstallerOption : ITranspilable
    {
        private const string DEFAULT_FORMAT = "{0}";
        public string Key { get; protected set; }
        protected string CustomFormatting { get; private set; }
        public abstract object[] Values { get; }

        public InstallerOption(string key)
        {
            Key = key;
            CustomFormatting = DEFAULT_FORMAT;
        }

        public InstallerOption(string key, string customFormatting)
        {
            Key = key;
            CustomFormatting = customFormatting;
        }

        public bool HasCustomFormat()
        {
            return DEFAULT_FORMAT != CustomFormatting;
        }

        public abstract string Transpile();
    }

    public abstract class InstallerOption<Tvalue> : InstallerOption
    {
        public Tvalue Value { get; protected set; }

        public InstallerOption(string key, Tvalue value) : base(key)
        {
            Value = value;
        }

        public InstallerOption(string key, Tvalue value, string customFormatting) : base(key, customFormatting)
        {
            Value = value;
        }

        public virtual void UpdateValue(Tvalue value)
        {
            Value = value;
        }
    }
}