﻿namespace HamerSoft.Wizard.Core.Options
{
    public interface ITranspilable
    {
        object[] Values { get; }
        string Transpile();
    }
}