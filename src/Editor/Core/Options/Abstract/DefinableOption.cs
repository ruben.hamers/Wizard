﻿namespace HamerSoft.Wizard.Core.Options
{
    public interface DefinableOption
    {
        string Define();
    }
}