﻿namespace HamerSoft.Wizard.Core.Options
{
    public class BooleanOption : InstallerOption<bool>
    {
        public override object[] Values => new object[] {Key, Value};

        public BooleanOption(string key, bool value) : base(key, value)
        {
        }

        public override string Transpile()
        {
            return $"{Key}={(Value ? "yes" : "no")}";
        }
    }
}