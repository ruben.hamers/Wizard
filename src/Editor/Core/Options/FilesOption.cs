namespace HamerSoft.Wizard.Core.Options
{
    public class FilesOption : ITranspilable
    {
        private readonly string _path;
        private readonly bool _overwrite;
        public object[] Values => new object[] {_path, _overwrite};

        public FilesOption(string path, bool overwrite)
        {
            _path = path;
            _overwrite = overwrite;
        }

        public string Transpile()
        {
            return $"Source: \"{_path}\"; DestDir: \"{{app}}\"; Flags: skipifsourcedoesntexist{(_overwrite ? " ignoreversion" : string.Empty)}";
        }
    }
}