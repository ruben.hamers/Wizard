﻿namespace HamerSoft.Wizard.Core.Options
{
    public class LanguageOption : InstallerOption
    {
        public override object[] Values => new[] {Key};

        public LanguageOption(string key) : base(key)
        {
        }

        public override string Transpile()
        {
            return $"Name: \"{Key.ToLower()}\"; MessagesFile: \"{(Key.ToLower() == "english" ? "compiler:Default.isl" : $"compiler:Languages\\{Key}.isl")}\"";
        }
    }
}