﻿using System;

namespace HamerSoft.Wizard.Core.Options
{
    public class DefineOption : InstallerOption<string>, DefinableOption
    {
        public string DefineKey => $"My{Key}";
        public override object[] Values => new object[] {Key, Value, CustomFormatting};

        public DefineOption(string key, string value) : base(key, value)
        {
        }

        public DefineOption(string key, string value, string format) : base(key, value, format)
        {
        }

        public string Define()
        {
            return $"#define {DefineKey} \"{string.Format(CustomFormatting, Value)}\"";
        }

        public override string Transpile()
        {
            return String.Empty;
        }
    }
}