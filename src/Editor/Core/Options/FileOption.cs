using System;
using System.IO;
using UnityEngine;

namespace HamerSoft.Wizard.Core.Options
{
    public class FileOption : InstallerOption<string>
    {
        private readonly string _fileExtension;
        private readonly bool _unityLocal;
        public override object[] Values => new object[] { Key, Value, _fileExtension, _unityLocal };

        public FileOption(string key, string value, string fileExtension, bool unityLocal = false) : base(key, value)
        {
            _fileExtension = fileExtension;
            _unityLocal = unityLocal;
        }

        public override string Transpile()
        {
            if (!Path.HasExtension(Value) || !File.Exists(Value) || !Value.EndsWith(_fileExtension))
                throw new Exception($"Selected file in FileOption {Key} does not exist or does not have the required file extension {_fileExtension}! ({Value})");
            return $"{Key}=\"{(_unityLocal ? $"{Application.dataPath}{Value.Replace("Assets", "")}" : Value)}\"";
        }
    }
}