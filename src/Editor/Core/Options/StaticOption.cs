namespace HamerSoft.Wizard.Core.Options
{
    public class StaticOption : InstallerOption
    {
        public override object[] Values => new object[] {Key};

        public StaticOption(string key) : base(key)
        {
        }

        public override string Transpile()
        {
            return Key;
        }
    }
}