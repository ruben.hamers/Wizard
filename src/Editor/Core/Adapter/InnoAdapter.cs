﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Wizard.Core.Adapter
{
    public static class InnoAdapter
    {
        private static string BaseUrl = "https://jrsoftware.org/";
        private static Version Current, Latest;

        public static Version GetCurrentVersion(bool force = false)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                return null;
            }

            if (Current != null && !force)
                return Current;

            var version = Cli.GetVersion();
            Current = !string.IsNullOrWhiteSpace(version) ? new Version(version) : null;
            return Current;
        }

        /// <summary>
        /// #Hackerman!
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="force"></param>
        public static void GetLatestVersion(Action<Version> callback, bool force = false)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                callback?.Invoke(null);
            }

            if (Latest != null && !force)
            {
                callback?.Invoke(Latest);
                return;
            }

            new WebRequest($"{BaseUrl}ishelp/topic_whatisinnosetup.htm").Get((code, data) =>
            {
                StringReader sr = new StringReader(data);
                var line = sr.ReadLine();
                var version = string.Empty;
                while (line != null)
                {
                    if (line.StartsWith("<b>Inno Setup version"))
                    {
                        version = line.Split(' ').Last().Replace("</b>", "").Replace("<br/>", "");
                        break;
                    }

                    line = sr.ReadLine();
                }

                Latest = Version.TryParse(version, out var latestVersion) ? latestVersion : null;
                callback?.Invoke(Latest);
            });
        }

        public static void HasLatestVersion(Action<bool> callback, bool force = false)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                callback?.Invoke(false);
            }

            if (Current != null && Latest != null && !force)
            {
                callback?.Invoke(Current.Equals(Latest));
                return;
            }

            GetCurrentVersion(force);
            GetLatestVersion(version => { callback?.Invoke(Current != null && Current.Equals(Latest)); }, force);
        }

        public static void DownloadInno(Action<string> callback)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                callback?.Invoke(null);
            }

            var destinationPath = Path.Combine(Application.persistentDataPath, "Inno-Installers", "InnoSetup.exe");
            new WebRequest($"{BaseUrl}download.php/is.exe").Download(destinationPath, (code, path) => { callback?.Invoke(code < 300 ? path : null); });
        }

        public static bool InstallInno(string path)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                return false;
            }

            int exitCode = 0;
            Cli.RunExe(path, code => { exitCode = code; });
            return exitCode == 0 && GetCurrentVersion(true) != null;
        }

        public static void UpdateInnoSetup(Action<bool> callback, bool force = false)
        {
            if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer)
            {
                Debug.LogWarning($"Cannot Run Wizard on {Application.platform}. Please use a Windows machine!");
                callback?.Invoke(false);
            }

            if (force)
                DownloadInno(path => { callback?.Invoke(InstallInno(path)); });
            else
                HasLatestVersion(hasLatest =>
                {
                    if (!hasLatest)
                        DownloadInno(path => { callback?.Invoke(InstallInno(path)); });
                    else
                        callback?.Invoke(true);
                });
        }
    }
}