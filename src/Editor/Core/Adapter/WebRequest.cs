﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace HamerSoft.Wizard.Core.Adapter
{
    public class WebRequest
    {
        private enum RequestState
        {
            Get,
            Download
        }

        private readonly string _url;
        private Action<long, string> _callback;
        private UnityWebRequest _www;
        private RequestState _state;
        private string _destinationPath;

        public WebRequest(string url)
        {
            _url = url;
        }

        public void Get(Action<long, string> callback)
        {
            _callback = callback;
            _www = UnityWebRequest.Get(_url);
            _state = RequestState.Get;
            _www.Send();
            EditorApplication.update += EditorUpdate;
        }

        public void Download(string destinationPath, Action<long, string> callback)
        {
            _destinationPath = destinationPath;
            _callback = callback;
            _www = UnityWebRequest.Get(_url);
            _state = RequestState.Download;
            _www.Send();
            EditorApplication.update += EditorUpdate;
        }

        private void EditorUpdate()
        {
            if (!_www.isDone)
                return;

            EditorApplication.update -= EditorUpdate;
            if (_www.isNetworkError)
            {
                _callback.Invoke(_www.responseCode, _www.error);
            }
            else
            {
                FinishRequest();
            }
        }

        private void FinishRequest()
        {
            switch (_state)
            {
                case RequestState.Get:
                    _callback.Invoke(_www.responseCode, _www.downloadHandler.text);
                    break;
                case RequestState.Download:
                    WriteOutFile();
                    _callback.Invoke(_www.responseCode, _destinationPath);
                    break;
            }
        }

        private void WriteOutFile()
        {
            var directory = Path.GetDirectoryName(_destinationPath);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            using (var fs = new FileStream(_destinationPath, FileMode.Create, FileAccess.ReadWrite))
                fs.Write(_www.downloadHandler.data, 0, _www.downloadHandler.data.Length);
        }
    }
}