﻿using System.Collections.Generic;
using System.Text;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public class Languages : Section
    {
        public override string Name => "Languages";
        public Languages()
        {
            Options.Add("English", new LanguageOption("English"));
        }

        public void AddLanguage(string language)
        {
            if (string.IsNullOrWhiteSpace(language))
                return;
            language = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(language.ToLower());
            Options[language] = new LanguageOption(language);
        }

        public void RemoveLanguage(string language)
        {
            if (string.IsNullOrWhiteSpace(language))
                return;
            language = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(language.ToLower());
            if (Options.ContainsKey(language))
                Options.Remove(language);
        }

        public override string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"[{Name}]");
            foreach (var languageOption in Options)
                builder.AppendLine(languageOption.Value.Transpile());

            return builder.ToString();
        }
    }
}