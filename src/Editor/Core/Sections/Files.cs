﻿using System.Collections.Generic;
using System.Text;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public class Files : Section
    {
        public override string Name => "Files";

        private readonly Dictionary<string, ITranspilable> _requiredDirs = new Dictionary<string, ITranspilable>
        {
            {"Exe", new FilesOption("{#MyBuildDir}\\{#MyProductName}.exe", true)},
            {"Dll", new FilesOption("{#MyBuildDir}\\UnityPlayer.dll", true)},
            {"CrashHandler64", new FilesOption("{#MyBuildDir}\\UnityCrashHandler64.exe", true)},
            {"CrashHandler32", new FilesOption("{#MyBuildDir}\\UnityCrashHandler32.exe", true)},
            {"Data", new DirectoryOption("{#MyBuildDir}", "{#MyProductName}_Data", true)},
            {"MonoBleedingEdge", new DirectoryOption("{#MyBuildDir}", "MonoBleedingEdge", true)},
            {"StreamingAssets", new DirectoryOption("{#MyBuildDir}", "StreamingAssets", true)},
            // IL2CPP files
            {"BaseLibDll", new FilesOption("{#MyBuildDir}\\baselib.dll", true)},
            {"GameAssemblyDll", new FilesOption("{#MyBuildDir}\\GameAssembly.dll", true)},
        };

        public Files()
        {
        }

        public void SetOptionalFile(string path, bool overwrite)
        {
            Options[path] = new FilesOption(path, overwrite);
        }

        public void RemoveOptionalFile(string path)
        {
            if (Options.ContainsKey(path))
                Options.Remove(path);
        }

        public override T GetOption<T>(string key)
        {
            var value = base.GetOption<T>(key);
            if (value == default && !string.IsNullOrWhiteSpace(key) && _requiredDirs.TryGetValue(key, out var option))
                return option as T;

            return value;
        }

        public override string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"[{Name}]");
            foreach (KeyValuePair<string, ITranspilable> fileOption in _requiredDirs)
                builder.AppendLine(fileOption.Value.Transpile());

            foreach (KeyValuePair<string, ITranspilable> fileOption in Options)
                builder.AppendLine(fileOption.Value.Transpile());

            return builder.ToString();
        }
    }
}