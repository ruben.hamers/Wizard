﻿using System.Collections.Generic;
using System.Text;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public class Run : Section
    {
        public override string Name => "Run";
        public Run()
        {
            Options.Add("Filename", new StaticOption("Filename: \"{app}\\{#MyAppExeName}\"; Description: \"{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}\"; Flags: nowait postinstall skipifsilent"));
        }

        public override string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"[{Name}]");
            foreach (KeyValuePair<string, ITranspilable> keyValuePair in Options)
                builder.AppendLine(keyValuePair.Value.Transpile());
            return builder.ToString();
        }
    }
}