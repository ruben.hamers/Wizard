﻿using System.Collections.Generic;
using System.Text;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public class Icons : Section
    {
        public override string Name => "Icons";
        public Icons()
        {
            Options.Add("autoPrograms", new StaticOption("Name: \"{autoprograms}\\{#MyAppName}\"; Filename: \"{app}\\{#MyAppExeName}\""));
            Options.Add("autoDesktop", new StaticOption("Name: \"{autodesktop}\\{#MyAppName}\"; Filename: \"{app}\\{#MyAppExeName}\"; Tasks: desktopicon"));
        }

        public override string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"[{Name}]");
            foreach (KeyValuePair<string, ITranspilable> transpilable in Options)
                builder.AppendLine(transpilable.Value.Transpile());
            return builder.ToString();
        }
    }
}