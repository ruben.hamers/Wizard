﻿using System.Collections.Generic;
using System.Text;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public class Tasks : Section
    {
        public override string Name => "Tasks";

        public Tasks()
        {
            Options.Add("DesktopIcon", new DesktopIcon());
        }

        public override string Transpile()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"[{Name}]");
            foreach (var transpilable in Options)
                builder.AppendLine(transpilable.Value.Transpile());

            return builder.ToString();
        }
    }
}