﻿using System.Collections.Generic;
using HamerSoft.Wizard.Core.Options;

namespace HamerSoft.Wizard.Core.Sections
{
    public abstract class Section : ITranspilable
    {
        public Dictionary<string, ITranspilable> Options;
        public abstract string Name { get; }
        public object[] Values { get; }

        public Section()
        {
            Options = new Dictionary<string, ITranspilable>();
        }

        public virtual T GetOption<T>(string key) where T : class, ITranspilable
        {
            if (!string.IsNullOrWhiteSpace(key) && Options.TryGetValue(key, out var option))
                return option as T;
            else
                return default;
        }

        public void SetOption(string key, ITranspilable option)
        {
            Options[key] = option;
        }

        public abstract string Transpile();
    }
}