﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HamerSoft.Wizard.IntegrationTests
{
    public class CliTests : IntegrationTest
    {
        private string _setupPath;
        private string _writtenPath;
        private string _buildPath;

        [SetUp]
        public void Setup()
        {
            _buildPath = GetBuildPath();
            _setupPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-Setup", $"{Application.productName}-Setup.exe");
            _writtenPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-Setup", $"{Application.productName}-InnoSetup.iss");
            if (File.Exists(_setupPath))
                File.Delete(_setupPath);
            if (File.Exists(_writtenPath))
                File.Delete(_writtenPath);
        }

        [Test]
        public void Installer_Locates_InnoSetup_ISCC()
        {
            if (ShouldRunTests())
                Assert.DoesNotThrow(() => { Cli.RunInnoCommand("/?").StartsWith("Usage"); });
            else
                Assert.True(true);
        }

        [Test]
        public void When_BuildPath_IsSet_ApplicationName_Is_Extracted_From_Files()
        {
            if (ShouldRunTests())
            {
                var installer = new Installer();
                Assert.True(installer.SetBuildDir(_buildPath));
                Assert.AreEqual("wizard", installer.GetSection<Setup>().GetOption<DefineOption>("ProductName").Value);
            }
            else
                Assert.True(true);
        }

        /// <summary>
        /// hacky test to check whether the help is invoked by checking the application.logmessage received callback...
        /// Timer is added to not run into a deadlock when no event is raised..
        ///
        /// -- Magic --
        ///
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator CompileSetup_Responds_To_Help()
        {
            if (!ShouldRunTests())
            {
                Assert.True(true);
                yield break;
            }

            bool helpReceived = false;

            void LogReceived(string logString, string stackTrace, LogType type)
            {
                if (!logString.StartsWith("Welcome to the Wizard help!"))
                    return;
                Application.logMessageReceived -= LogReceived;
                helpReceived = true;
                Assert.True(true);
            }

            Assert.DoesNotThrow(() =>
            {
                Application.logMessageReceived += LogReceived;
                CompileSetup(true, String.Empty, String.Empty, string.Empty);
            });
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var fiveSeconds = new TimeSpan(0, 0, 5);
            while (!helpReceived && stopwatch.Elapsed < fiveSeconds)
                yield return new WaitForSeconds(1);
            Assert.True(helpReceived);
        }

        /// <summary>
        /// hacky test to check whether the help is invoked by checking the application.logmessage received callback...
        /// Timer is added to not run into a deadlock when no event is raised..
        ///
        /// -- Magic --
        ///
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator WriteOutInnoFile_Responds_To_Help()
        {
            if (!ShouldRunTests())
            {
                Assert.True(true);
                yield break;
            }

            bool helpReceived = false;

            void LogReceived(string logString, string stackTrace, LogType type)
            {
                if (!logString.StartsWith("Welcome to the Wizard help!"))
                    return;
                Application.logMessageReceived -= LogReceived;
                helpReceived = true;
                Assert.True(true);
            }

            Assert.DoesNotThrow(() =>
            {
                Application.logMessageReceived += LogReceived;
                WriteOutFile(true, String.Empty, String.Empty);
            });
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var fiveSeconds = new TimeSpan(0, 0, 5);
            while (!helpReceived && stopwatch.Elapsed < fiveSeconds)
                yield return new WaitForSeconds(1);
            Assert.True(helpReceived);
        }

        [Test]
        public void When_BuildPath_And_Output_Are_Set_The_Compilation_Succeeds()
        {
            if (!ShouldRunTests())
            {
                // not running my windows device! need to fix this for future tests
                Assert.True(true);
                return;
            }

            Assert.DoesNotThrow(() =>
            {
                CompileSetup(false, _buildPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-Setup"), string.Empty);
                Assert.True(File.Exists(_setupPath));
            });
        }

        [Test]
        public void When_BuildPath_And_Output_Are_Set_The_WriteOut_Succeeds()
        {
            if (!ShouldRunTests())
            {
                // not running my windows device! need to fix this for future tests
                Assert.True(true);
                return;
            }

            Assert.DoesNotThrow(() =>
            {
                WriteOutFile(false, _buildPath, _writtenPath);
                Assert.True(File.Exists(_writtenPath));
            });
        }

        [Test]
        public void When_Raw_Inno_File_Is_Supplied_The_InnoFile_Takes_Precedence()
        {
            if (!ShouldRunTests())
            {
                // not running my windows device! need to fix this for future tests
                Assert.True(true);
                return;
            }

            var installer = new Installer();
            installer.SetBuildDir(_buildPath);
            string rawPath = installer.WriteOut();
            Assert.DoesNotThrow(() =>
            {
                CompileSetup(false, String.Empty, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-Setup"), rawPath);
                Assert.True(File.Exists(_setupPath));
            });
        }

        private int CompileSetup(bool help, string buildPath, string outputDir, string innoFile)
        {
            return (int) typeof(Cli).GetMethod("CompileSetup", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] {help, buildPath, outputDir, innoFile});
        }

        private int WriteOutFile(bool help, string buildPath, string outputDir)
        {
            return (int) typeof(Cli).GetMethod("WriteOutInnoFile", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] {help, buildPath, outputDir});
        }

        [TearDown]
        public void TearDown()
        {
            if (File.Exists(_setupPath))
                File.Delete(_setupPath);
        }
    }
}