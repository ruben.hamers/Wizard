﻿using System.IO;
using System.Linq;
using HamerSoft.Wizard.Core;
using UnityEngine;

namespace HamerSoft.Wizard.IntegrationTests
{
    public abstract class IntegrationTest
    {
        private readonly string[] _roots = new string[]
        {
            $"{Application.dataPath}/../Build",
            $"{Application.dataPath}/../Build/Windows",
            $"{Application.dataPath}/../Builds",
            $"{Application.dataPath}/../Builds/Windows",
        };

        protected bool ShouldRunTests()
        {
            return Application.platform == RuntimePlatform.WindowsEditor && !Cli.ArgExists("-batchmode");
        }

        protected string GetBuildPath()
        {
            return _roots.FirstOrDefault(r => File.Exists(Path.Combine(r, $"{Application.productName}.exe")));
        }
    }
}