﻿using System;
using System.IO;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Wizard.IntegrationTests
{
    public class InstallerTests : IntegrationTest
    {
        private Installer _installer;

        [SetUp]
        public void Setup()
        {
            _installer = new Installer();
        }

        [Test]
        public void Run_Writes_The_Inno_File_To_Disk()
        {
            if (ShouldRunTests())
            {
                _installer.SetBuildDir(GetBuildPath());
                _installer.Run(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), $"{Application.productName}-Setup"));
            }
            else
            {
                Assert.True(true);
            }
        }

        [Test]
        public void Upon_Construction_Default_Sections_Are_Set()
        {
            if (ShouldRunTests())
                Assert.AreNotEqual(string.Empty, _installer.Transpile());
            else
                Assert.True(true);
        }

        [Test]
        public void Transpile()
        {
            if (ShouldRunTests())
            {
                _installer.SetSection(new Setup("TestWizard", "1.0.0", " HamerSoft"));
                _installer.SetSection(new Icons());
                _installer.SetSection(new Run());
                _installer.SetSection(new Tasks());
                var languages = new Languages();
                languages.AddLanguage("Dutch");
                languages.AddLanguage("Deutsch");
                _installer.SetSection(languages);
                _installer.SetSection(new Files());
                Debug.Log(_installer.Transpile());
                Assert.True(true);
            }
            else

                Assert.True(true);
        }
    }
}