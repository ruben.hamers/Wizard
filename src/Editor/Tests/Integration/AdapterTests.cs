﻿using System;
using System.Collections;
using System.IO;
using HamerSoft.Wizard.Core;
using HamerSoft.Wizard.Core.Adapter;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HamerSoft.Wizard.IntegrationTests
{
    public class AdapterTests : IntegrationTest
    {
        [Test]
        public void B_When_Inno_Install_Is_Called_The_Exe_Starts()
        {
            if (ShouldRunTests())
                Assert.True(InnoAdapter.InstallInno(Path.Combine(Application.persistentDataPath, "Inno-Installers", "InnoSetup.exe")));
            else
                Assert.True(true);
        }

        [Test]
        public void GetCurrentVersion_Returns_UnInstallers_Version()
        {
            if (ShouldRunTests())
            {
                var version = InnoAdapter.GetCurrentVersion(true);
                Debug.Log(version);
                Assert.NotNull(version);
            }
            else
            {
                Assert.True(true);
            }
        }

        [UnityTest]
        public IEnumerator C_GetLatestVersion_Returns_A_Version_Greater_Than_0_0_0_0()
        {
            if (ShouldRunTests())
            {
                var requestComplete = false;
                InnoAdapter.GetLatestVersion(version =>
                {
                    Debug.Log(version);
                    Assert.NotNull(version);
                    Assert.True(version >= new Version(6, 1, 2));
                    requestComplete = true;
                }, true);
                while (!requestComplete)
                    yield return null;
            }
            else
            {
                Assert.True(true);
                yield return null;
            }
        }

        [UnityTest]
        public IEnumerator HasLatestVersion_Returns_True_When_Installed_Latest()
        {
            if (ShouldRunTests())
            {
                var requestComplete = false;
                InnoAdapter.HasLatestVersion(success =>
                {
                    Assert.True(success);
                    requestComplete = true;
                }, true);
                while (!requestComplete)
                    yield return null;
            }
            else
            {
                Assert.True(true);
                yield return null;
            }
        }

        [UnityTest]
        public IEnumerator A_Download_Version_Downloads_The_Latest_Inno_Exe()
        {
            if (ShouldRunTests())
            {
                var requestComplete = false;
                InnoAdapter.DownloadInno(path =>
                {
                    Assert.True(path.StartsWith(Application.persistentDataPath));
                    requestComplete = true;
                });
                while (!requestComplete)
                    yield return null;
            }
            else
            {
                Assert.True(true);
                yield return null;
            }
        }

        [UnityTest]
        public IEnumerator D_Update_InnoSetup_Does_Not_Install_Newer_Version_If_Latest_Is_Installed()
        {
            var current = InnoAdapter.GetCurrentVersion();
            if (ShouldRunTests())
            {
                var requestComplete = false;
                InnoAdapter.UpdateInnoSetup(success =>
                {
                    Assert.True(success);
                    Assert.AreEqual(current, InnoAdapter.GetCurrentVersion());
                    requestComplete = true;
                });
                while (!requestComplete)
                    yield return null;
            }
            else
            {
                Assert.True(true);
                yield return null;
            }
        }
    }
}