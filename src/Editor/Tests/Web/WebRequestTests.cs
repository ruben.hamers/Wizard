﻿using System;
using System.Timers;
using NUnit.Framework;
using UnityEngine;
using WebRequest = HamerSoft.Wizard.Core.Adapter.WebRequest;

namespace HamerSoft.Wizard.Tests.Web
{
    public class WebRequestTests
    {
        [Test]
        public void WebRequest_Returns_Html_Code()
        {
            var request = new WebRequest("https://jrsoftware.org/");
            Timer timer = null;
            RunWithTimer(5000, () =>
            {
                request.Get((code, text) =>
                {
                    Debug.Log(code);
                    Assert.True(text.StartsWith("<!DOCTYPE html"));
                    timer.Stop();
                });
            }, out timer);
        }

        private void RunWithTimer(int duration, Action method, out Timer timer)
        {
            timer = new Timer(duration * 1000);
            method?.Invoke();
            timer.Start();
        }
    }
}