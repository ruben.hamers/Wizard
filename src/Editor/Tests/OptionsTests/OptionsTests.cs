﻿using HamerSoft.Wizard.Core.Options;
using JetBrains.Annotations;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Wizard.Tests.Options
{
    public class OptionsTests
    {
        [Test]
        public void DefineOption_Transpiles_To_KeyValue()
        {
            var def = new DefinedOption("AppName", "TestApp");
            Assert.AreEqual("AppName", def.Key);
            Assert.AreEqual("TestApp", def.Value);
            Assert.AreEqual("MyAppName", def.DefineKey);
            Assert.AreEqual("AppName={#MyAppName}", def.Transpile());
            Assert.AreEqual("#define MyAppName \"TestApp\"", def.Define());
        }

        [Test]
        public void LanguageOption_English_Transpiles_To_Default()
        {
            var option = new LanguageOption("English");
            Assert.AreEqual("English", option.Key);
            Assert.AreEqual("Name: \"english\"; MessagesFile: \"compiler:Default.isl\"", option.Transpile());
        }

        [Test]
        public void BooleanOption_Transpiles_To_yes_When_True_no_When_False()
        {
            var trueOption = new BooleanOption("Foo", true);
            var falseOption = new BooleanOption("Bar", false);
            Assert.AreEqual("Foo", trueOption.Key);
            Assert.IsTrue(trueOption.Value);
            Assert.AreEqual("Foo=yes", trueOption.Transpile());

            Assert.AreEqual("Bar", falseOption.Key);
            Assert.IsFalse(falseOption.Value);
            Assert.AreEqual("Bar=no", falseOption.Transpile());
        }


        [Test]
        public void LanguageOption_ANY_Transpiles_CorrectLanguage()
        {
            var option = new LanguageOption("Dutch");
            Assert.AreEqual("Dutch", option.Key);
            Assert.AreEqual("Name: \"dutch\"; MessagesFile: \"compiler:Languages\\Dutch.isl\"", option.Transpile());
        }

        [Test]
        public void FileOption_Transpiles_To_CorrectPath()
        {
            var option = new FilesOption("Foo/bar.baz", false);
            var optionOverwrite = new FilesOption("Foo/bar.baz", true);

            Assert.AreEqual("Source: \"Foo/bar.baz\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist", option.Transpile());
            Assert.AreEqual("Source: \"Foo/bar.baz\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion", optionOverwrite.Transpile());
        }

        [Test]
        public void PersistentDataOption_Transpiles_Directory_Correctly()
        {
            var option = new PersistentDataOption("fake path", "FooBar", true);
            Assert.AreEqual($"Source: \"{Application.persistentDataPath}\\FooBar\\*\"; DestDir: \"{{userappdata}}\\..\\LocalLow\\{{#MyAppPublisher}}\\{{#MyProductName}}\\FooBar\"; Flags: skipifsourcedoesntexist recursesubdirs createallsubdirs ignoreversion",
                option.Transpile());
        }

        [Test]
        public void PersistentDataOption_Transpiles_File_Correctly()
        {
            var option = new PersistentDataOption("fake path", "FooBar.exe", true);
            Assert.AreEqual($"Source: \"{Application.persistentDataPath}\\FooBar.exe\"; DestDir: \"{{userappdata}}\\..\\LocalLow\\{{#MyAppPublisher}}\\{{#MyProductName}}\\FooBar.exe\"; Flags: skipifsourcedoesntexist ignoreversion", option.Transpile());
        }
    }
}