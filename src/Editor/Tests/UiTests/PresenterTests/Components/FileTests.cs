using System.Collections.Generic;
using System.IO;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using HamerSoft.Wizard.Ui.Components.File;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using File = HamerSoft.Wizard.Ui.Components.File.File;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class FileTests : ComponentTest<FilePresenter, File, Object>
    {
        private Texture _wizardImage;
        private string _imagePath;

        [SetUp]
        public void Setup()
        {
            _wizardImage = Resources.Load<Texture>("WizardSetupImages/WizardImageFile");
            _imagePath = AssetDatabase.GetAssetPath(_wizardImage);
        }

        [Test]
        public void File_Only_Sets_Value_When_AssetExists_And_Matches_FileExtension()
        {
            Presenter = new FilePresenter(new SerializableOption()
            {
                Values = new List<string> {"Foo", "", ".bmp"}
            }, "Tooltip");
            Assert.AreEqual(string.Empty, Presenter.Option.Values[1]);
            ForceSetData(_wizardImage);
            Assert.AreEqual(_imagePath, Presenter.Option.Values[1]);
        }
    }
}