using System;
using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class PersistentDataTests : ComponentTest<PersistentDataPresenter, PersistentData, Tuple<string, bool>>
    {
        [Test(Description = "Testing that the PersistentDataPath option is displayed correctly")]
        public void When_Option_2_IS_Bool_It_Is_Converted_Correctly()
        {
            var trueOption = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Qux", "MyPath", "True"}};
            var truePresenter = new PersistentDataPresenter(trueOption, "Tooltip!", dataPresenter => { });
            Assert.True(truePresenter.Drawable.Value.Item2);
            var falseOption = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Qux", "MyPath", "False"}};
            var falsePresenter = new PersistentDataPresenter(falseOption, "Tooltip!", dataPresenter => { });
            Assert.False(falsePresenter.Drawable.Value.Item2);
        }

        [Test(Description = "Testing when PersistentDataOption is serialized with invalid paramters the overwrite flag is initialized to false")]
        public void When_Option_2_Cannot_Be_Converted_To_Bool_False_Will_Be_Used()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Qux", "MyPath", "yeeeeeesyeeeesyes"}};
            var presenter = new PersistentDataPresenter(option, "Tooltip!", dataPresenter => { });
            Assert.False(presenter.Drawable.Value.Item2);
        }

        [Test(Description = "Testing when the data is set, both values in the tuple are changed.")]
        public void SetData_Sets_Both_Values_Of_The_Tuple()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Qux", "MyPath", "False"}};
            Presenter = new PersistentDataPresenter(option, "Tooltip!", dataPresenter => { });
            ForceSetData(new Tuple<string, bool>("updated", true));
            Assert.AreEqual("updated", Presenter.Option.Values[1]);
            Assert.AreEqual("True", Presenter.Option.Values[2]);
        }
    }
}