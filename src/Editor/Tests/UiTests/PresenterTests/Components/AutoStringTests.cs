using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class AutoStringTests : ComponentTest<AutoStringPresenter, AutoString, string>
    {
        [Test]
        public void UponConstruction_AutoStringPresenter_Does_Not_Set_Value_Automatically_When_Values_Are_Not_Equal()
        {
            var presenter = new AutoStringPresenter(new SerializableOption()
            {
                Values = new List<string> {"Foo", "Bar", "{0}"}
            }, "Mock string", () => "Magic");
            Assert.AreEqual("Bar", presenter.Drawable.Value);
            Assert.AreEqual("Bar", presenter.Option.Values[1]);
        }

        [Test]
        public void AutoStringPresenter_Sets_Value_Automatically_When_Auto_Is_Enabled()
        {
            var presenter = new AutoStringPresenter(new SerializableOption()
            {
                Values = new List<string> {"Foo", "Magic", "{0}"}
            }, "Mock string", () => "Magic");
            Assert.AreEqual(true, presenter.Drawable.Automatic);
        }

        [Test]
        public void Validator_Function_Parses_Value_When_Set()
        {
            Presenter = new AutoStringPresenter(new SerializableOption()
            {
                Values = new List<string> {"Foo", "Magic", "{0}"}
            }, "Mock string", () => "Magic", s =>
            {
                Assert.AreEqual("Magic", s);
                return s;
            });
            ForceSetData("Magic");
        }
    }
}