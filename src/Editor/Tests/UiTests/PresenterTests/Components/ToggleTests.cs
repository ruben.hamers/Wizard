using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class ToggleTests : ComponentTest<TogglePresenter, Toggle, bool>
    {

        [Test]
        public void When_Option_1_Is_Bool_Toggle_Value_Is_Set()
        {
            var trueOption = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "True"}};
            var truePresenter = new TogglePresenter(trueOption, "Tooltip");
            Assert.True(truePresenter.Drawable.Value);
            
            var falseOption = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "False"}};
            var falsePresenter = new TogglePresenter(falseOption, "Tooltip");
            Assert.False(falsePresenter.Drawable.Value);
        }

        [Test]
        public void When_Option_1_Is_Not_Converteable_To_Bool_False_Is_Set()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "yeeeeees yeees yes"}};
            var presenter = new TogglePresenter(option, "Tooltip");
            Assert.False(presenter.Drawable.Value);
        }
    }
}