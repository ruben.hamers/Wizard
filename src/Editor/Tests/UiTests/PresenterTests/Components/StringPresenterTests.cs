using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class StringPresenterTests : ComponentTest<StringPresenter, String, string>
    {
        [Test]
        public void Validator_Function_Parses_Value_When_Set()
        {
            Presenter = new StringPresenter(new SerializableOption()
            {
                Values = new List<string> {"Foo", "Magic", "{0}"}
            }, "Mock string", s =>
            {
                Assert.AreEqual("Magic", s);
                return s;
            });
            ForceSetData("Magic");
        }
    }
}