using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Components
{
    public class SelectionTests : ComponentTest<SelectionPresenter, Selection, int>
    {
        [Test]
        public void When_Current_Value_Is_Included_In_Options_It_Is_Set()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "Bar"}};
            var presenter = new SelectionPresenter(option, "Tooltip", new[] {"Foo", "Bar", "Baz"});
            Assert.AreEqual(1, presenter.Drawable.Value);
        }

        [Test]
        public void When_Current_Value_Is_Not_Included_In_Options_It_Is_Set_To_0()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "Qux"}};
            var presenter = new SelectionPresenter(option, "Tooltip", new[] {"Foo", "Bar", "Baz"});
            Assert.AreEqual(0, presenter.Drawable.Value);
        }

        [Test]
        public void When_Options_Is_Null_And_Empty_Array_Is_Used()
        {
            var option = new SerializableOption {Key = "Foo", Type = "Bar", Values = new List<string> {"Foo", "Bar"}};
            var presenter = new SelectionPresenter(option, "Tooltip", null);
            Assert.AreEqual(0, presenter.Drawable.Value);
        }
    }
}