using System.Collections.Generic;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests
{
    public class BasePresenterTest
    {
        private class MockComponent : UiComponent<string>
        {
            public MockComponent(string label, string value) : base(label, value)
            {
            }

            public override void Draw()
            {
            }

            public void SetValue(string value)
            {
                Value = value;
            }
        }

        private class MockPresenter : UiComponentPresenter<MockComponent, string>
        {
            public MockPresenter(SerializableOption option, string tooltip) : base(option, tooltip)
            {
                Drawable = CreateComponent(option);
                Drawable.SetToolTip(tooltip);
            }

            protected override MockComponent CreateComponent(SerializableOption option)
            {
                return new MockComponent(option.Key, option.Values[1]);
            }
        }

        public class BaseTests
        {
            private MockPresenter _mock;

            [SetUp]
            public void Setup()
            {
                _mock = new MockPresenter(new SerializableOption()
                {
                    Key = "Foo",
                    Type = "MockPresenter",
                    Values = new List<string> {"Foo", "Bar", "Baz"}
                }, "This is a mock!");
            }

            [Test]
            public void When_Data_Is_Set_The_Value_In_The_SerializeOption_Is_Overwritten()
            {
                _mock.Drawable.SetValue("test string");
                _mock.Draw();
                Assert.AreEqual("test string", _mock.Option.Values[1]);
            }

            [Test]
            public void When_Data_And_There_Is_A_ValidationFunction_The_Data_Is_Validated()
            {
                _mock.SetValidator(value => value.StartsWith("test") ? value.Replace("test", "") : value);
                _mock.Drawable.SetValue("test string");
                _mock.Draw();
                Assert.AreEqual(" string", _mock.Option.Values[1]);
                _mock.Drawable.SetValue("FooBar");
                _mock.Draw();
                Assert.AreEqual("FooBar", _mock.Option.Values[1]);
            }
        }
    }
}