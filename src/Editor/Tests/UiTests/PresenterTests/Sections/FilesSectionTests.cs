using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Serializable;
using HamerSoft.Wizard.Ui;
using HamerSoft.Wizard.Ui.Components;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Wizard.Tests.UiTests.PresenterTests.Sections
{
    public class FilesSectionTests
    {
        private class MockFilesPresenter : FilesPresenter
        {
            public MockFilesPresenter(SerializableSection serializableSection) : base(serializableSection)
            {
            }

            public void CreateOption()
            {
                typeof(FilesPresenter).GetMethod("CreateNewOption", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(this, null);
                Components = CreateComponents(SerializableSection.Options.ToDictionary(key => key.Key, value => value));
            }

            public List<Presenter> GetComponents()
            {
                return Components;
            }
        }

        [Test]
        public void When_PersistentData_Is_Added_It_Is_Added_To_The_Components()
        {
            var presenter = new MockFilesPresenter(new SerializableSection("Foo", new List<SerializableOption>()));
            presenter.CreateOption();
            Assert.AreEqual(1, presenter.GetData().Options.Count);
        }

        [Test]
        public void When_PersistentData_Is_Added_It_Correctly_Configured()
        {
            var presenter = new MockFilesPresenter(new SerializableSection("Foo", new List<SerializableOption>()));
            presenter.CreateOption();
            var option = presenter.GetData().Options.First();
            Assert.AreEqual(typeof(PersistentDataOption).FullName, option.Type);
            Assert.True(System.Guid.TryParse(option.Key, out var parsed));
            Assert.AreEqual(Application.persistentDataPath, option.Values[0]);
            Assert.AreEqual(string.Empty, option.Values[1]);
            Assert.True(Boolean.Parse(option.Values[2]));
        }

        [Test]
        public void When_Option_Delete_IsTriggered_It_Is_Removed_From_The_Section()
        {
            var presenter = new MockFilesPresenter(new SerializableSection("Foo", new List<SerializableOption>()));
            presenter.CreateOption();
            Assert.AreEqual(1, presenter.GetData().Options.Count);
            var persistentDataPresenter = presenter.GetComponents().First() as PersistentDataPresenter;
            var removeFn = typeof(PersistentDataPresenter).GetField("_removeCallback", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(persistentDataPresenter) as Action<PersistentDataPresenter>;
            removeFn?.Invoke(persistentDataPresenter);
            Assert.AreEqual(0, presenter.GetData().Options.Count);
        }
    }
}