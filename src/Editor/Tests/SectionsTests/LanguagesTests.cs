using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.Options
{
    public class LanguagesTests
    {
        private Languages _languages;

        [SetUp]
        public void Setup()
        {
            _languages = new Languages();
        }

        [Test]
        public void English_Is_Always_Added_By_Default()
        {
            Assert.NotNull(_languages.GetOption<LanguageOption>("English"));
        }

        [Test]
        public void English_Transpiles_With_Default_CompilerISL_File()
        {
            Assert.AreEqual("Name: \"english\"; MessagesFile: \"compiler:Default.isl\"", _languages.GetOption<LanguageOption>("English").Transpile());
        }

        [Test]
        public void Added_Languages_Are_Capitalized()
        {
            _languages.AddLanguage("dutch");
            Assert.AreEqual("Dutch",_languages.GetOption<LanguageOption>("Dutch").Key);
        }
        
    }
}