using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.Options
{
    public class RunTests
    {
        private Run _run;

        [SetUp]
        public void Setup()
        {
            _run = new Run();
        }

        [Test]
        public void Upon_Construction_FileName_Line_Is_Set()
        {
            Assert.AreEqual("Filename: \"{app}\\{#MyAppExeName}\"; Description: \"{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}\"; Flags: nowait postinstall skipifsilent",_run.GetOption<StaticOption>("Filename").Transpile());
        }
    }
}