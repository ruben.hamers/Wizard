﻿using System.IO;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Wizard.Tests.Options
{
    public class SetupTests
    {
        private Setup _setup;

        [SetUp]
        public void Setup()
        {
            _setup = new Setup("TestApp", "1.0.0", "HamerSoft");
        }

        [Test]
        public void Upon_Construction_Required_Fields_Are_Generated()
        {
            Assert.NotNull(_setup.GetOption<StringOption>("AppId").Value);
            Assert.AreEqual("TestApp", _setup.GetOption<DefinedOption>("AppName").Value);
            Assert.AreEqual("TestApp", _setup.GetOption<DefineOption>("AppExeName").Value);
            Assert.AreEqual("1.0.0", _setup.GetOption<DefinedOption>("AppVersion").Value);
            Assert.AreEqual("HamerSoft", _setup.GetOption<DefinedOption>("AppPublisher").Value);
            Assert.AreEqual("https://HamerSoft.com", _setup.GetOption<DefinedOption>("AppPublisherUrl").Value);
            Assert.NotNull(_setup.GetOption<StringOption>("DefaultDirName").Value);
            Assert.AreEqual("dialog", _setup.GetOption<StringOption>("PrivilegesRequiredOverridesAllowed").Value);
            Assert.AreEqual("Setup", _setup.GetOption<StringOption>("OutputBaseFilename").Value);
            Assert.AreEqual("lzma", _setup.GetOption<StringOption>("Compression").Value);
            Assert.AreEqual(false, _setup.GetOption<BooleanOption>("SolidCompression").Value);
            Assert.AreEqual("modern", _setup.GetOption<StringOption>("WizardStyle").Value);
            Assert.AreEqual(true, _setup.GetOption<BooleanOption>("Uninstallable").Value);
        }

        [Test]
        public void UpdateOption_Updates_DefinedOption()
        {
            _setup.UpdateOption("AppName", "Updated");
            Assert.AreEqual("Updated", _setup.GetOption<DefinedOption>("AppName").Value);
        }

        [Test]
        public void UpdateOption_Preserves_Formatting_WhenDefined_DefinedOption()
        {
            Assert.AreEqual("#define MyAppExeName \"TestApp.exe\"", _setup.GetOption<DefineOption>("AppExeName").Define());
            _setup.UpdateOption("AppExeName", "Updated");
            Assert.AreEqual("#define MyAppExeName \"Updated.exe\"", _setup.GetOption<DefineOption>("AppExeName").Define());
        }

        [Test]
        public void SetupImages_Are_Relative_Paths()
        {
            if (Application.platform != RuntimePlatform.WindowsEditor)
                return;
            Debug.Log(_setup.GetOption<FileOption>("SetupIconFile").Value);
            Assert.False(Path.IsPathRooted(_setup.GetOption<FileOption>("SetupIconFile").Value));

            Debug.Log(_setup.GetOption<FileOption>("WizardImageFile").Value);
            Assert.False(Path.IsPathRooted(_setup.GetOption<FileOption>("WizardImageFile").Value));

            Debug.Log(_setup.GetOption<FileOption>("WizardSmallImageFile").Value);
            Assert.False(Path.IsPathRooted(_setup.GetOption<FileOption>("WizardSmallImageFile").Value));
        }
    }
}