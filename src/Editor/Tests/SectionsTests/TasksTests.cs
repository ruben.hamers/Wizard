using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.Options
{
    public class TasksTests
    {
        private Tasks _tasks;

        [SetUp]
        public void Setup()
        {
            _tasks = new Tasks();
        }

        [Test]
        public void Upon_Construction_Desktop_Icon_Is_Set()
        {
            Assert.AreEqual("Name: \"desktopicon\"; Description: \"{cm:CreateDesktopIcon}\"; GroupDescription: \"{cm:AdditionalIcons}\"; Flags: unchecked",_tasks.GetOption<DesktopIcon>("DesktopIcon").Transpile());
        }
    }
}