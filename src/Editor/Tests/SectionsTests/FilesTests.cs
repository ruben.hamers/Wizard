using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.Options
{
    public class FilesTests
    {
        private Files _files;

        [SetUp]
        public void Setup()
        {
            _files = new Files();
        }

        [Test]
        public void When_RootFolder_Is_Set_Required_Dirs_And_Files_Are_Added()
        {
            Assert.AreEqual("Source: \"{#MyBuildDir}\\{#MyProductName}.exe\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion",_files.GetOption<FilesOption>("Exe").Transpile());
            Assert.AreEqual("Source: \"{#MyBuildDir}\\UnityPlayer.dll\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion",_files.GetOption<FilesOption>("Dll").Transpile());
            Assert.AreEqual("Source: \"{#MyBuildDir}\\UnityCrashHandler64.exe\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion",_files.GetOption<FilesOption>("CrashHandler64").Transpile());
            Assert.AreEqual("Source: \"{#MyBuildDir}\\UnityCrashHandler32.exe\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion",_files.GetOption<FilesOption>("CrashHandler32").Transpile());
            Assert.AreEqual("Source: \"{#MyBuildDir}\\{#MyProductName}_Data\\*\"; DestDir: \"{app}\\{#MyProductName}_Data\"; Flags: recursesubdirs createallsubdirs skipifsourcedoesntexist ignoreversion",_files.GetOption<DirectoryOption>("Data").Transpile());
            Assert.AreEqual("Source: \"{#MyBuildDir}\\MonoBleedingEdge\\*\"; DestDir: \"{app}\\MonoBleedingEdge\"; Flags: recursesubdirs createallsubdirs skipifsourcedoesntexist ignoreversion",_files.GetOption<DirectoryOption>("MonoBleedingEdge").Transpile());
        }

        [Test]
        public void Custom_Files_Are_Added_As_Well()
        {
            _files.SetOptionalFile("Foo\\Bar.Baz", true);
            _files.SetOptionalFile("Dir1\\File2.foo", false);
            Assert.AreEqual("Source: \"Foo\\Bar.Baz\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist ignoreversion",_files.GetOption<FilesOption>("Foo\\Bar.Baz").Transpile());
            Assert.AreEqual("Source: \"Dir1\\File2.foo\"; DestDir: \"{app}\"; Flags: skipifsourcedoesntexist",_files.GetOption<FilesOption>("Dir1\\File2.foo").Transpile());
        }
    }
}