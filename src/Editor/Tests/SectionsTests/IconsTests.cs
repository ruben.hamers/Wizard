using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.Options
{
    public class IconsTests
    {
        private Icons _icons;

        [SetUp]
        public void Setup()
        {
            _icons = new Icons();
        }

        [Test]
        public void Upon_Construction_Default_Icon_Commands_Are_Added()
        {
            Assert.AreEqual("Name: \"{autoprograms}\\{#MyAppName}\"; Filename: \"{app}\\{#MyAppExeName}\"", _icons.GetOption<StaticOption>("autoPrograms").Key);
            Assert.AreEqual("Name: \"{autodesktop}\\{#MyAppName}\"; Filename: \"{app}\\{#MyAppExeName}\"; Tasks: desktopicon", _icons.GetOption<StaticOption>("autoDesktop").Key);
        }
    }
}