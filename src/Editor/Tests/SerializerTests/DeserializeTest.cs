using System.Collections.Generic;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Serializable;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.SerializerTests
{
    public class DeserializeTest : AbstractSerializerTest
    {
        [Test]
        public void SerializedOption_DeSerializes_Correctly_To_StringOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string>() {"Foo", "Bar"},
                Type = typeof(StringOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as StringOption;
            Assert.AreEqual("Foo", deserializedOption.Key);
            Assert.AreEqual("Bar", deserializedOption.Value);
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_DefinedOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "Bar", "{0}{1}"},
                Type = typeof(DefinedOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as DefinedOption;
            Assert.AreEqual("Foo", deserializedOption.Key);
            Assert.AreEqual("Bar", deserializedOption.Value);
            Assert.IsTrue(deserializedOption.HasCustomFormat());
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_DirectoryOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "Bar", "True"},
                Type = typeof(DirectoryOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as DirectoryOption;
            Assert.AreEqual($"Source: \"Foo\\Bar\\*\"; DestDir: \"{{app}}\\Bar\"; Flags: recursesubdirs createallsubdirs skipifsourcedoesntexist ignoreversion", deserializedOption.Transpile());
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_FileOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "Bar", ".baz", "True"},
                Type = typeof(FileOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as FileOption;
            Assert.AreEqual("Foo", deserializedOption.Values[0]);
            Assert.AreEqual("Bar", deserializedOption.Values[1]);
            Assert.AreEqual(".baz", deserializedOption.Values[2]);
            Assert.AreEqual(true, deserializedOption.Values[3]);
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_FilesOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "True"},
                Type = typeof(FilesOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as FilesOption;
            Assert.AreEqual($"Source: \"Foo\"; DestDir: \"{{app}}\"; Flags: skipifsourcedoesntexist ignoreversion", deserializedOption.Transpile());
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_LanguageOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo"},
                Type = typeof(LanguageOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as LanguageOption;
            Assert.AreEqual("Foo", deserializedOption.Key);
        }

        [Test]
        public void SerializableOption_Serializes_Correctly_To_BooleanOption()
        {
            var option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "True"},
                Type = typeof(BooleanOption).FullName
            };

            var deserializedOption = Deserializer.Deserialize(option) as BooleanOption;
            Assert.AreEqual("Foo", deserializedOption.Key);
            Assert.IsTrue(deserializedOption.Value);


            option = new SerializableOption
            {
                Key = "Foo",
                Values = new List<string> {"Foo", "False"},
                Type = typeof(BooleanOption).FullName
            };
            deserializedOption = Deserializer.Deserialize(option) as BooleanOption;
            Assert.AreEqual("Foo", deserializedOption.Key);
            Assert.IsFalse(deserializedOption.Value);
        }
    }
}