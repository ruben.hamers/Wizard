using System.Text;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.SerializerTests
{
    public class AbstractSerializerTest
    {
        protected class MockSection : Section
        {
            public override string Name => "Mock";

            public override string Transpile()
            {
                StringBuilder sb = new StringBuilder();
                foreach (var option in Options)
                    sb.AppendLine(option.Value.Transpile());
                return sb.ToString();
            }
        }


        protected MockSection Section;

        [SetUp]
        public virtual void Setup()
        {
            Section = new MockSection();
        }
    }
}