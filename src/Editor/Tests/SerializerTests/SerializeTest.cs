using System;
using HamerSoft.Wizard.Core.Options;
using HamerSoft.Wizard.Core.Sections;
using HamerSoft.Wizard.Serializable;
using NUnit.Framework;

namespace HamerSoft.Wizard.Tests.SerializerTests
{
    public class SerializeTest : AbstractSerializerTest
    {
        [Test]
        public void StringOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new StringOption("Foo", "Bar");
            var serializedOption = Serializer.Serialize(option.Key, option);

            Assert.AreEqual("Foo", serializedOption.Key);
            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("Bar", serializedOption.Values[1]);
        }

        [Test]
        public void DefinedOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new DefinedOption("Foo", "Bar");
            var serializedOption = Serializer.Serialize(option.Key, option);

            Assert.AreEqual("Foo", serializedOption.Key);
            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("Bar", serializedOption.Values[1]);
        }

        [Test]
        public void DirectoryOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new DirectoryOption("Foo", "Bar", true);
            var serializedOption = Serializer.Serialize("Foo", option);

            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("Bar", serializedOption.Values[1]);
            Assert.AreEqual("True", serializedOption.Values[2]);
        }

        [Test]
        public void FileOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new FileOption("Foo", "Bar", ".baz", true);
            var serializedOption = Serializer.Serialize("Foo", option);

            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("Bar", serializedOption.Values[1]);
            Assert.AreEqual(".baz", serializedOption.Values[2]);
            Assert.AreEqual("True", serializedOption.Values[3]);
        }

        [Test]
        public void FilesOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new FilesOption("Foo", true);
            var serializedOption = Serializer.Serialize("Foo", option);

            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("True", serializedOption.Values[1]);
        }

        [Test]
        public void LanguageOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new LanguageOption("Foo");
            var serializedOption = Serializer.Serialize("Foo", option);

            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
        }

        [Test]
        public void BooleanOption_Serializes_Correctly_To_SerializedOption()
        {
            var option = new BooleanOption("Foo", true);
            var serializedOption = Serializer.Serialize("Foo", option);

            Assert.AreEqual(option.GetType().FullName, serializedOption.Type);
            Assert.AreEqual("Foo", serializedOption.Values[0]);
            Assert.AreEqual("True", serializedOption.Values[1]);
        }

        [Test]
        public void MockSection_Serializes_Correctly()
        {
            Section.SetOption("Foo", new StringOption("Bar", "Baz"));
            Section.SetOption("Bool", new BooleanOption("Qux", false));
            var serializedSection = Serializer.Serialize(Section);
            Assert.AreEqual("HamerSoft.Wizard.Tests.SerializerTests.AbstractSerializerTest+MockSection", serializedSection.Type);
            Assert.AreEqual("Baz", serializedSection.Options[0].Values[1]);
            Assert.AreEqual("False", serializedSection.Options[1].Values[1]);
        }
    }
}