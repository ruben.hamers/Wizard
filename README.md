# Wizard

#### Don't be a noob and level up your deployment game to become a true Wizard!

[comment]: <> (![]&#40;/Docs~/pw.gif&#41;)
<img src="Docs~/pw.gif"  width="120" height="120">

** Magic **

# Introduction
This is a Unity3D plugin to compile custom made windows installer setups, based on [InnoSetup](https://jrsoftware.org/isinfo.php).
With Wizard you can configure settings for InnoSetup straight in your Unity3D editor and compile a setup wizard for all your windows applications.
You are able to compile setup files manually from you editor and through commandline arguments if you are running some sort of build pipeline.

<i>Never again will you deliver clumsy .zip archives to your customers!</i> 

# What is InnoSetup
So first of all, what is InnoSetup!? Their homepage says this: 

``Inno Setup is a free installer for Windows programs by Jordan Russell and Martijn Laan. First introduced in 1997, Inno Setup today rivals and even surpasses many commercial installers in feature set and stability.``
So basically InnoSetup is a piece of software to gather files and aggregate them into a single .exe file for installation on your windows OS.
Everyone who has ever used the windows OS is familiar with the concept of setup wizards. They are the standard for installing new software on a device.

With Wizard you can compile these setup wizards using some simple configurations from your Unity3D editor!


For more information please check [InnoSetup](https://jrsoftware.org/isinfo.php)

<i><b>*For now I only added options I thought were relevant for simple Unity3D projects, if you are missing an option please make a ticket [here](https://gitlab.com/ruben.hamers/UnityNstaller/-/boards) or implement it yourself and shoot me a merge/pull request!</b></i>

# Creating a Config
So the first thing you need to do is create a config. This is a simple ScriptableObject (SO) and you can create it from the menu item: `HamerSoft -> Wizard -> Create Config`.

![ConfigImage](Docs~/CreateConfig.png)

Then a new SO called `Config` will appear in a directory called `Assets/Resources/Wizard/`.
When you select the config you will get see it in the inspector like this (probably with a different AppName and Publisher).

![ConfigImage](Docs~/WizardConfig.jpg)

Now, you have this config and you are ready to compile a setup wizard based on the default values already filled in.

# Configuring a config
Next you need to configure the config to your liking.
Some properties are automatically generated based on settings in the Unity3D editor. You can disable the `Automatic` toggle to edit them manually.

<i>*If you are familiar with InnoSetup you might notice that settings are far from complete. I only implemented settings that I thought were important.

**Also note that some settings are hidden but they are implemented under the hood like for example: offering the user the choice to create a desktop icon after installation. I think this is pretty standard behaviour so it is always included.

***All elements in the UI allow hover functionality to show some tooltip with information.</i>

In the inspector of the config you can see different toggle groups which are called "Sections".
Each Section corresponds to it's original InnoSetup counterpart. (See the InnoSetup [help](https://jrsoftware.org/ishelp/), `Setup Script Sections` specifically.)

## Setup
This section contains global settings used by the installer and uninstaller. Certain directives are required for any installation you create, these are all configurable from Unity3D.
These settings include a name for you application, exe file and publisher information. Also you need to mark your application uninstallable if you want to.
You can also select images to include in the installer.

## Languages

The languages section allows you to add different languages for the steps in the setup wizard.
As a default, I added English, you can't remove it. I think English is probably the main, or fallback language in many cases.
InnoSetup supports many languages, you can simply toggle them in the inspector and they will be included.
If you cannot find the language you are looking for, it's not supported and you might want to create a feature request on InnoSetup's website yourself.
## Tasks

The Tasks Section defines all of the user-customizable tasks Setup will perform during installation. In the current defaults the user is offered a choice to install the application just for the current user or system-wide.
I added this task per default since I think some end users might not have the correct rights to install applications system-wide.
Currently, this is hardcoded. If you do not like this, shoot in a ticket please.

## Files

The files section is the section that is most interesting. This section defines the files and directories that should be copied and installed on the user's platform.
In this section, all files that come as output from a Unity3D windows build are included by default.
The default files include:

|File/Directory|Description|Required|
|:------|:-----:|-----:|
|</MyApplication/>.exe|The executable file of your application| yes|
|UnityPlayer.dll|The Unity Player ddl output from your compiled application| yes|
|CrashHandler32/64>|Unity's 'crash handler', either 32 or 64 bit depending on your build|yes|
|</MyApplication/>_Data/*|The Data directory of your application| yes|
|MonoBleedingEdge/*|Mono folder of your compiled application|yes|
|StreamingAssets/*|If you are using StreamingAssets the folder will be included automatically| no|

In the files section you can also add custom directories that point to your `Application.PersistentDataPath`. 
I added this option since it is often used to add additional data to a build where it is accessible to Unity3d immediately.

![FilesImage](Docs~/FilesSection.jpg)

**For now it's not possible to add custom, rooted paths. It's not difficult to add this but I just did not since it does not make much sense since the paths most likely differ when other users on your team use Wizard.

## Icons

The Icons section allows shortcuts for an installation like; creating a Desktop icon and adding your application to the quick-launch menu.
I only added the option to add a desktop icon here, which comes by default and cannot be changed through the UI.
I think everyone would want a desktop icon of your awesome installed application. (If not, please shoot a feature request in!)

## Run

The Run section is optional, and specifies any number of programs to execute after the program has been successfully installed, but before the Setup program displays the final dialog.
Currently it only prompts the user to run the application after it is installed.
However I could add options to run like a README.txt, or changelog.txt but I haven't added this yet.
Again, if you need it, hit me up!

# Installing InnoSetup

To compile the config into a correct .iss (InnoSetup) file. You need to install InnoSetup on your windows PC.
You can do this through the menu in your Unity3D editor `HamerSoft -> Wizard -> Check For Updates`. When you run this Wizard will check whether you have InnoSetup Installed and if the version is the latest.
If not, you are prompted with the InnoSetup Install wizard (Inception!). This might take a short period of time since I need to download the .exe file from InnoSetup's website.

## Checking for updates

There are two options here, you either check for updates and only install InnoSetup if the version is dated, or you force an update for whatever reason.
You can choose both options from the `HamerSoft -> Wizard` menu item.

# Compiling a setup

Now that you have installed or updated your InnoSetup Installation you can compile the config into a new setup wizard .exe file.
I added two options again, you can either install it from the Unity3D editor or you can use a build pipeline to compile yourself a .exe

## In the editor

Ok, so you can open an Editor Window from the menu bar at the `HamerSoft -> Wizard -> Compile Setup` menu item.
When you click this the following window will open:

![CompileImage](Docs~/EditorCompileWindow.jpg)

Here you can select the directory of your existing Unity3D build. This is the folder that contains the default files I described in the Files Section.
Once you hit the `Confirm` button it will start the compilation of the wizard and it will open the `Desktop` folder automatically, which is where the wizard will be saved.

## From the commandline ( i.e. pipeline)

You are also able to hook this all up on your existing build pipeline by adding some new stage during deployment.
Wizard allows a couple of parameters to start a compiled Unity3D build or run the editor with additional startup commands.

|Command|Description|Required|
|:------|:-----:|-----:|
|-wizardHelp|Show the help text in the console (see below)| no|
|-compile-wizard|keyword to trigger a compile| yes|
|-buildPath|The folder your build is located| yes|
|-outputDir|The directory you want the compiled setup .exe to be stored| yes|
|-raw|If you have a predefined .iss file, you can add the path to that file here and it will use that for installation| no|

```
Welcome to the Wizard help!
There are a couple of important parameters you need to remember.
------
-wizardHelp: Shows this information block.
-compile-wizard: keyword to trigger a compile.
-writeout-wizard: keyword to trigger a write out of the .iss file.
-buildPath: The path where your Unity3D Windows build is currently at.
-outputDir: The output directory to place the compiled SetupWizard.
-raw: Path to a raw InnoSetup file (.iss) to run.
------
*     -outputDir is always required.
**    order of arguments do not matter!.
***   A raw input file always has precedence over a configured Wizard in Unity3D since it is probably set for a reason!.
****  All paths must be rooted paths!
***** Either -compile-wizard or -writeout-wizard not both.

An example command for compiling a new setup based on the settings configured in Unity3D would be:
-compile-wizard -buildpath <path> -outputDir <myOutputDir>

Another command to run a pre-existing InnoSetup (.iss) file would be:
-compile-wizard -raw <myIssFile> -outputDir <myOutputDir>

An example command for generating a new .iss file based on the settings configured in Unity3D would be:
-writeout-wizard -buildpath <path> -outputDir <myOutputDir>
```

### Example startup commands for the **Editor**
If you want to create a setup using the editor you need to start a Unity3D editor with the following startup commands. 

`"<PathToUnity>\Unity.exe" -quit -projectPath "<PathToProject>\MyProject" -batchMode -buildpath <path> -outputDir <myOutputDir> -executeMethod CLI.CompileSetup`

*-batchMode is not required, yet it will start your app headless which is less stressful on your pipeline since it does not use a gpu.

### Example startup commands for **Build**
If you do not have access to an installed Unity3D editor you can compile a setup file from a build as well.

`"<PathToProject>\MyProject.exe" -batchMode -compile-wizard -buildpath <path> -outputDir <myOutputDir>`

# .iss File Output
If, for some reason you do not want to generate a setup wizard directly you can also choose to only output the InnoSetup source code (.iss) file.
There are again, two ways of doing this. 

## In the editor
Ok, so you can open an Editor Window from the menu bar at the `HamerSoft -> Wizard -> WriteOut .iss File` menu item.
Once selected, a window will open where you can select your build path.

### From the commandline in the **Editor**

`"<PathToUnity>\Unity.exe" -quit -batchmode -projectPath "<PathToProject>\MyProject" -buildpath <path> -outputDir <myOutputDir> -executeMethod CLI.WriteOutInnoFile`

### Example startup commands for **Build**

`"<PathToProject>\MyProject.exe" -batchMode -writeout-wizard -buildpath <path> -outputDir <myOutputDir>`

# Tests

There are some tests available to run. I split the up in two categories, either Unit-Tests or Integration tests.
You can run both categories however, the Integration tests will require input from you, as a user. I added them to make sure some processes work as expected.
For example: After you check for updates, and install InnoSetup (which is a manual task, I cannot automate that) the correct callback is invoked.

Also the integration tests assume that there is a build of your project at the paths defined [here](src/Editor/Tests/Integration/IntegrationTest.cs).

# Feature Requests & Bugs
Please report new feature requests and bugs [here](https://gitlab.com/ruben.hamers/UnityNstaller/-/boards)